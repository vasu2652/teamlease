import TsConfigPathsPlugin from "tsconfig-paths-webpack-plugin";
import webpack from "webpack";

const config: webpack.Configuration = {
	entry: {
		chatbot: "./index.tsx"
	},
	output: {
		path: __dirname,
		filename: "dist/main.bundle.js"
	},

	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: "source-map-loader"
			},
			{
				test: /\.jsx?$/,
				loader: "source-map-loader"
			},
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				loader: "ts-loader"
			}
		]
	},
	resolve: {
		extensions: [".tsx", ".ts", ".js", ".jsx"],
		plugins: [
			new TsConfigPathsPlugin({
				extensions: [".tsx", ".ts", ".js", ".jsx"] // Should be provided again!
			})
		]
	},

	mode: "development",
	devtool: "source-map"
};

export default config;
