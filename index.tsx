import { App } from "App/App";
import * as React from "react";
import * as ReactDOM from "react-dom";
import chrono from "chrono-node";
(window as any).chrono = chrono;

class Wrapper extends React.Component<any, any> {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			msg: true,
			maxWidth: window.innerWidth
		};
	}
	// componentDidMount() {
	// 	this.setState({
	// 	})
	// }
	render() {
		return (
			<div style={{ position: "fixed", bottom: 0, right: 0, zIndex: 10000 }}>
				<div
					style={{
						backgroundColor: "#003A90",
						position: "absolute",
						bottom: 20,
						right: 20,
						padding: "20px",
						borderRadius: "100%",
						boxShadow: "0px 0px 10px #003D8C",
						cursor: "pointer"
					}}
					onClick={() => {
						// Toggle State.
						this.setState({ show: !this.state.show });
					}}
				>
					<svg
						style={{
							opacity: this.state.show ? 0 : 1,
							transition: "0.4s all",
							stroke: "#003A90",
							fill: "white"
							// zIndex: 10000
						}}
						width={36}
						xmlns="http://www.w3.org/2000/svg"
						version="1.1"
						id="Capa_1"
						x="0px"
						y="0px"
						viewBox="0 0 473 473"
					>
						<g>
							<path d="M403.581,69.3c-44.7-44.7-104-69.3-167.2-69.3s-122.5,24.6-167.2,69.3c-86.4,86.4-92.4,224.7-14.9,318 c-7.6,15.3-19.8,33.1-37.9,42c-8.7,4.3-13.6,13.6-12.1,23.2s8.9,17.1,18.5,18.6c4.5,0.7,10.9,1.4,18.7,1.4 c20.9,0,51.7-4.9,83.2-27.6c35.1,18.9,73.5,28.1,111.6,28.1c61.2,0,121.8-23.7,167.4-69.3c44.7-44.7,69.3-104,69.3-167.2 S448.281,114,403.581,69.3z M384.481,384.6c-67.5,67.5-172,80.9-254.2,32.6c-5.4-3.2-12.1-2.2-16.4,2.1c-0.4,0.2-0.8,0.5-1.1,0.8 c-27.1,21-53.7,25.4-71.3,25.4h-0.1c20.3-14.8,33.1-36.8,40.6-53.9c1.2-2.9,1.4-5.9,0.7-8.7c-0.3-2.7-1.4-5.4-3.3-7.6 c-73.2-82.7-69.4-208.7,8.8-286.9c81.7-81.7,214.6-81.7,296.2,0C466.181,170.1,466.181,302.9,384.481,384.6z" />
							<circle cx="236.381" cy="236.5" r="16.6" />
							<circle cx="321.981" cy="236.5" r="16.6" />
							<circle cx="150.781" cy="236.5" r="16.6" />
						</g>
					</svg>
					{this.state.msg && (
						<div
							style={{
								position: "absolute",
								bottom: "calc(100% - 10px)",
								right: "calc(100% - 10px)",
								filter: "drop-shadow(0px 0px 5px grey)",
								visibility: "hidden"
							}}
						>
							<i
								style={{
									position: "absolute",
									padding: 20,
									top: 0,
									right: 0
								}}
								onClick={e => {
									this.setState({ msg: false });
									e.stopPropagation();
								}}
							>
								<svg
									xmlns="http://www.w3.org/2000/svg"
									version="1.1"
									id="Capa_1"
									x="0px"
									y="0px"
									width={12}
									height={12}
									viewBox="0 0 348.333 348.334"
									style={{
										fill: "#848484"
									}}
								>
									<path d="M336.559,68.611L231.016,174.165l105.543,105.549c15.699,15.705,15.699,41.145,0,56.85 c-7.844,7.844-18.128,11.769-28.407,11.769c-10.296,0-20.581-3.919-28.419-11.769L174.167,231.003L68.609,336.563 c-7.843,7.844-18.128,11.769-28.416,11.769c-10.285,0-20.563-3.919-28.413-11.769c-15.699-15.698-15.699-41.139,0-56.85 l105.54-105.549L11.774,68.611c-15.699-15.699-15.699-41.145,0-56.844c15.696-15.687,41.127-15.687,56.829,0l105.563,105.554 L279.721,11.767c15.705-15.687,41.139-15.687,56.832,0C352.258,27.466,352.258,52.912,336.559,68.611z" />
								</svg>
							</i>
							<img src="http://afrikasun.co.za/images/chatinc.gif" />
						</div>
					)}
				</div>

				<div
					style={{
						// padding: 20
						maxWidth: this.state.maxWidth,
						transition: "0.5s all",
						transformOrigin: "right bottom",
						display: this.state.show?"block":"none",
						transform: this.state.show ? `scale(1)` : "scale(0)",
						opacity: this.state.show ? 1 : 0,
						overflow: "hidden",
						pointerEvents: this.state.show?"auto":"none",
						// padding: 17,
						borderRadius: this.state.show ? `5px` : `100%`
					}}
				>
					<App
						onClose={() => {
							this.setState({ show: false });
						}}
					/>
				</div>
			</div>
		);
	}
}

const elem = document.createElement("div");
elem.id = 'divchatbot';
document.body.appendChild(elem);
ReactDOM.render(<Wrapper />, elem);
