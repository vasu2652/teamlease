import TsConfigPathsPlugin from "tsconfig-paths-webpack-plugin";
import webpack from "webpack";

const config: webpack.Configuration = {
	entry: {
		chatbot: "./index.tsx"
	},
	output: {
		path: __dirname,
		filename: "bundle/[name].[contenthash].js"
	},

	module: {
		rules: [
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				loader: "ts-loader"
			}
		]
	},
	resolve: {
		extensions: [".tsx", ".ts", ".js", ".jsx"],
		plugins: [
			new TsConfigPathsPlugin({
				configFile: "./tsconfig.publish.json",
				extensions: [".tsx", ".ts", ".js", ".jsx"] // Should be provided again!
			})
		]
	},

	mode: "production"
};

export default config;
