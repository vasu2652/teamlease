import CssBaseline from "@material-ui/core/CssBaseline";
import _ from "lodash";
import React from "react";
import { IFlowGraph, INodeMap } from "./ChatBot/Flow/schema";
import { ChatBot } from "./ChatBot/index";
import { api_data, flowGraph, NodeMap } from "./ChatBot/sample";
import { ITemplate, Template } from "./ChatBot/template_helper";

interface IAppState {
	FlowGraph?: IFlowGraph;
	nodeMap?: INodeMap;
}
// "https://bot.sayint.ai/teamlease/api/api/getData/chatbot_flow?filterBy={}"
export class App extends React.Component<any, IAppState> {
	state = { FlowGraph: undefined, nodeMap: undefined };
	constructor(props: any) {
		super(props);
		fetch(`https://bot.sayint.ai/teamlease/api/api/getData/chatbot_flow?filterBy={}`).then(response => {
			response.json().then(data => {
				console.log("DATA", data);
				data = _.get(data, "payload.datasetsFetchData[0].key");
				// console.log("UPDATE", data);
				const template = new Template(data);
				const config = template.getConfig("tl_flow_init_choices_v1");
				// const config = template.getConfig("247_flow_init");
				// console.log(config);
				this.setState({
					nodeMap: config.nodeMap,
					FlowGraph: config.flow
				});
			});
		});
	}
	render() {
		const { FlowGraph, nodeMap } = this.state;
		if (FlowGraph && nodeMap) {
			return (
				<CssBaseline>
					<ChatBot
						onClose={this.props.onClose}
						flowGraph={FlowGraph}
						nodeMap={nodeMap}
					/>
				</CssBaseline>
			);
		}
		return <div>Loading...</div>;
	}
}
