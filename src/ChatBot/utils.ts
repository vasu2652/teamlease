export const DelayBuffer = delay => {
	const funcs: { func: any; immediate: boolean; timeout: number }[] = [];
	let notRunning = true;
	const schedule = () => {
		notRunning = false;
		const config = funcs.shift();
		if (config) {
			// Func is there.
			if (config.immediate) {
				config.func();
				schedule();
			} else {
				setTimeout(() => {
					config.func();
					schedule();
				}, config.timeout);
			}
		} else {
			notRunning = true;
		}
	};
	return (func, immediate = false) => {
		funcs.push({
			func,
			timeout: delay,
			immediate
		});
		if (notRunning) {
			schedule();
		}
	};
};
