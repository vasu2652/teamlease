import { IFlowGraph, INode, INodeMap } from "App/ChatBot/Flow/schema";
import { ITemplate } from "App/ChatBot/template_helper";
import _ from "lodash";

export const api_data: ITemplate = {
	graphs: {
		r_base_flow: {
			id: "r_base_flow",
			graph: {
				node: "r_node_bot_begin",
				edges: [
					{
						node: "r_node_interested",
						edges: [
							{
								node: "r_node_final_questions"
							},
							{
								node: "r_node_follow_up_questions",
								edges: [
									{
										node: "r_node_final_questions"
									}
								]
							}
						]
					},
					{
						node: "r_node_not_interested"
					}
				]
			}
		}
	},
	nodes: {
		r_node_bot_begin: {
			id: "r_node_bot_begin",
			flow: null,
			texts: {},
			actions: [
				"r_action_bm_greeting",
				"r_action_bm_jd",
				"r_action_bm_interested"
			]
		},
		r_node_interested: {
			id: "r_node_interested",
			flow: null,
			texts: {
				option: "Yes, I’m Interested."
			},
			actions: [
				"r_action_in_relevant_experience",
				"r_action_in_skills_widget",
				"r_action_async_analyze_rating"
			]
		},
		r_node_not_interested: {
			id: "r_node_not_interested",
			flow: null,
			texts: {
				option: "No, not right now."
			},
			actions: ["r_action_bm_not_interested"]
		},
		r_node_final_questions: {
			id: "r_node_final_questions",
			flow: null,
			texts: {},
			actions: [
				"r_action_in_current_ctc",
				"r_action_in_expected_ctc",
				"r_action_in_notice_period",
				"r_action_bm_end_flow"
			]
		},
		r_node_follow_up_questions: {
			id: "r_node_follow_up_questions",
			flow: null,
			texts: {},
			actions: ["r_action_in_fq_1", "r_action_in_fq_2"]
		}
	},
	actions: {
		r_action_bm_jd: {
			id: "r_action_bm_jd",
			type: "BOT_MESSAGE",
			params: {
				name: "r_action_bm_jd",
				texts: {
					message:
						"Clipjoy is a well funded startup with a high energy team to make a difference in the way India consumes video. We are looking for a passionate engineer/developer wanting to disrupt video viewing in India by stregenthening and scaling our core backend framework."
				}
			}
		},
		r_action_in_fq_1: {
			id: "r_action_in_fq_1",
			type: "INPUT_TEXT",
			params: {
				name: "r_action_in_fq_1",
				texts: {
					question:
						"Which is your preferred mdoe for real time media communication WebRTC or RTMP and why ?"
				}
			}
		},
		r_action_in_fq_2: {
			id: "r_action_in_fq_2",
			type: "INPUT_TEXT",
			params: {
				name: "r_action_in_fq_2",
				texts: {
					question:
						"What are some of the open source libraries that you have worked with for implementing scalable APIs ?"
				}
			}
		},
		r_action_bm_end_flow: {
			id: "r_action_bm_end_flow",
			type: "BOT_MESSAGE",
			params: {
				name: "r_action_bm_end_flow",
				texts: {
					message:
						"Thanks for providing this valuable information, our team will get in touch with you regarding further procedures."
				}
			}
		},
		r_action_bm_greeting: {
			id: "r_action_bm_greeting",
			type: "BOT_MESSAGE",
			params: {
				name: "r_action_bm_greeting",
				texts: {
					message: "Hi There, Welcome to Sayint Recruitment. "
				}
			}
		},
		r_action_bm_interested: {
			id: "r_action_bm_interested",
			type: "BOT_MESSAGE",
			params: {
				name: "r_action_bm_interested",
				texts: {
					message: "Are you interested in the offer ?"
				}
			}
		},
		r_action_in_current_ctc: {
			id: "r_action_in_current_ctc",
			type: "INPUT_TEXT",
			params: {
				name: "r_action_in_current_ctc",
				texts: {
					question: "What is your current CTC ?"
				}
			}
		},
		r_action_in_expected_ctc: {
			id: "r_action_in_expected_ctc",
			type: "INPUT_TEXT",
			params: {
				name: "r_action_in_expected_ctc",
				texts: {
					question: "What is your expected CTC ?"
				}
			}
		},
		r_action_in_notice_period: {
			id: "r_action_in_notice_period",
			type: "INPUT_TEXT",
			params: {
				name: "r_action_in_notice_period",
				texts: {
					question:
						"How long is the notice period at your current company, if yes is it negotiable ?"
				}
			}
		},
		r_action_in_skills_widget: {
			id: "r_action_in_skills_widget",
			type: "RATING",
			params: {
				name: "r_action_in_skills_widget",
				texts: {
					question:
						"How would you rate yourselves in the following skillsets ?"
				},
				options: ["Python", "Node JS", "Go"]
			}
		},
		r_action_bm_not_interested: {
			id: "r_action_bm_not_interested",
			type: "INPUT_TEXT",
			params: {
				name: "r_action_bm_not_interested",
				texts: {
					message:
						"Thanks for your time, we hope to peak your interest when the next offer comes along."
				}
			}
		},
		r_action_async_analyze_rating: {
			id: "r_action_async_analyze_rating",
			type: "ASYNCFLOW",
			params: {
				api: "/v1/recruitment/analyze_rating",
				name: "r_action_async_analyze_rating",
				texts: {
					loading: "Please wait whille we upload your rating"
				}
			}
		},
		r_action_in_relevant_experience: {
			id: "r_action_in_relevant_experience",
			type: "INPUT_TEXT",
			params: {
				name: "r_action_in_relevant_experience",
				texts: {
					question:
						"How many years of experience do you have in developing production ready micro services for serving media files ?"
				}
			}
		}
	}
};
const flow = [
	{
		node: "payslip",
		edges: [
			{
				node: "payslip_1month",
				edges: [],
				asOption: "Last month"
			},
			{
				node: "payslip_3months",
				edges: [],
				asOption: "Last 3 months"
			},
			{
				node: "payslip_6months",
				edges: [],
				asOption: "Last 6 months"
			}
		],
		asQuestion:
			"For how many months would you like to download payslip for?",
		asOption: "Payslip"
	},
	{
		node: "form16",
		edges: [
			{
				node: "form16_current",
				edges: [],
				asOption: "Current Fiscal Year."
			},
			{
				node: "form16_2years",
				edges: [],
				asOption: "Last 2 fiscal years."
			},
			{
				node: "form16_3years",
				edges: [],
				asOption: "Last 3 fiscal years"
			}
		],
		asQuestion: "Which Form 16 would you like to Download?",
		asOption: "Form 16"
	}
];
export const flowGraph: IFlowGraph = {
	edges: [
		{
			node: "securityQuestion1",
			edges: [
				{
					node: "securityQuestion2",
					edges: [
						{
							node: "flow",
							edges: flow,
							asQuestion: "What is it you're looking for?"
						},
						{
							node: "securityQuestion3",
							edges: [
								{
									node: "flow",
									edges: flow,
									asQuestion: "What is it you're looking for?"
								},
								{ node: "error", edges: [] }
							]
						}
					]
				}
			]
		}
	],
	asQuestion: "What is it you're looking for?",
	node: "BotBegin"
};

const download = {
	async payslip(months: number) {
		const response = await fetch(`http://172.16.6.101:7001/api/requestor`, {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				mode: "cors"
			},
			body: JSON.stringify({
				nlp_processed: [
					{
						salary_slip: _.times(months).map(n => ({
							month: "Aug",
							year: "2018"
						}))
					}
				],
				from_email: "irfan.shaik48@gmail.com",
				user: "",
				subject: "",
				snippet: "",
				internal_date: "",
				to_email: "",
				complete_message: "",
				thread_id: "",
				template: "",
				msg_id: "",
				date: "",
				history_id: "",
				message: ""
			})
		});
		const json = await response.json();

		const content = _.get(
			json,
			"template.classes.salary_slip.payload[0].FileBlob.Content"
		);
		return `data:application/octet-stream;base64,${content}`;
	},
	form16(years: number) {}
};

export const NodeMap: INodeMap = {
	BotBegin: {
		actions: [
			{ type: "BOT_MESSAGE", message: ["Hello User!"] },
			{
				type: "INPUT_TEXT",
				name: "name",
				question: ["What's your name?"]
			},
			{ type: "BOT_MESSAGE", message: data => [`Hi, ${data.name}`] }
		]
	},
	securityQuestion1: {
		actions: [
			// {
			// 	type: "OPTIONS",
			// 	question: "What are you looking for?",
			// 	name: "interest",
			// 	options: [
			// 		{
			// 			option: "Frontend",
			// 			value: "frontend"
			// 		},
			// 		{
			// 			option: "Backend",
			// 			value: "backend"
			// 		}
			// 	]
			// },
			// {
			// 	type: "RATING",
			// 	name: "Cool",
			// 	options: ["interest"],
			// 	question: d =>
			// 		`How much do you rate yourself in ${d.interest} tech?`
			// },
			{
				type: "INPUT_TEXT",
				name: "sq1",
				question: "Security Question 1!"
			}
		]
	},
	securityQuestion2: {
		actions: [
			{
				type: "INPUT_TEXT",
				name: "sq2",
				question: "SECURITY QUESTION2"
			},
			{
				type: "ASYNC",
				action: async data => {
					if (data.sq1 === "true" && data.sq2 === "true") {
						// Right answer!;
						return "flow";
					}
					return "securityQuestion3";
				}
			}
		]
	},
	securityQuestion3: {
		actions: [
			{
				type: "INPUT_TEXT",
				name: "sq3",
				question: "SECURITY QUESTION3"
			},
			{
				type: "ASYNC",
				action: async data => {
					const answers = [data.sq1, data.sq2, data.sq3];

					if (answers.filter(a => a === "true").length === 2) {
						// Right answer!;
						return "flow";
					}
					return "error";
				}
			}
		]
	},
	flow: {
		actions: [
			{
				type: "BOT_MESSAGE",
				message: "Welcome to Teamlease Chatbot!"
			}
		]
	},
	error: {
		actions: [
			{
				type: "BOT_MESSAGE",
				message: "Sorry, cannot move forward!"
			}
		]
	},
	payslip: {
		actions: []
	},
	payslip_1month: {
		actions: [
			{
				type: "BOT_MESSAGE",
				message: [
					"Please download payslip for last month from below link!"
				]
			},
			{
				type: "BOT_MESSAGE",
				message: async data => {
					const link = await download.payslip(1);
					return `<a href='${link}'>Payslip1</a>`;
				}
			}
		]
	},
	payslip_3months: {
		actions: [
			{
				type: "BOT_MESSAGE",
				message: [
					"Please download payslip for last 3 months from below link!"
				]
			}
		]
	},
	payslip_6months: {
		actions: [
			{
				type: "BOT_MESSAGE",
				message: [
					"Please download payslip for last 6 months from below link!"
				]
			}
		]
	},

	form16: {
		actions: []
	},
	form16_current: {
		actions: [
			{
				type: "BOT_MESSAGE",
				message: [
					"Please download payslip for last year from below link!"
				]
			}
		]
	},
	form16_2years: {
		actions: [
			{
				type: "BOT_MESSAGE",
				message: [
					"Please download Form16 for last 2 years from below link!"
				]
			}
		]
	},
	form16_3years: {
		actions: [
			{
				type: "BOT_MESSAGE",
				message: [
					"Please download Form16 for last 3 years from below link!"
				]
			}
		]
	}
};
