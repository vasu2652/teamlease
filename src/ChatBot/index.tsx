import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import LinearProgress from "@material-ui/core/LinearProgress";
import axios from "axios";

import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import CloseIcon from "@material-ui/icons/Close";
import RefreshIcon from "@material-ui/icons/Refresh";

import { Typography } from "@material-ui/core";
import indigo from "@material-ui/core/colors/indigo";

import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import Axios from "axios";
import _ from "lodash";
import React from "react";
import { ChatBotBase } from "./base";
import { addToken, IEvent, IInteraction } from "./Flow/action";
import { OptionQuestion, RatingQuestion, TextQuestion } from "./ux/Input";
import { BotMessage, UserMessage } from "./ux/Message";

const theme = createMuiTheme({
	palette: {
		primary: indigo
	},
	typography: {
		fontSize: 14,
		htmlFontSize: 14
	}
});

const styles = th =>
	({
		container: {
			display: "flex",
			flexWrap: "wrap"
		},
		textField: {
			marginLeft: th.spacing.unit,
			marginRight: th.spacing.unit,
			// height: 24,
			color: "#fff !important",
			width: 140
		},
		fontColor: {
			color: "#fff"
		},
		dense: {
			marginTop: 16
		},
		menu: {
			width: 200
		}
	} as any);

export const ChatBot = withStyles(styles)(
	class extends ChatBotBase<{ onClose: any; classes: any }, {}> {
		contentRef: any = React.createRef();
		logPosition: {
			cHeight: number;
			scrollHeight: number;
			oHeight: number;
			scrollTop: number;
		} = { cHeight: 0, scrollHeight: 0, oHeight: 0, scrollTop: 0 };
		constructor(props: any) {
			super(props);
			this.run = this.run.bind(this);
			this.onHistoryChange = _.debounce(this.onHistoryChange, 500, {
				trailing: true
			});
		}
		async run() {
			await this.flow.run();
			requestAnimationFrame(this.run);
		}
		componentDidMount() {
			this.run();
		}
		renderComponent(ev: IEvent) {
			const getMessage = msg => {
				try {
					const result = JSON.parse(msg);
					return result;
				} catch (e) {
					return msg;
				}
			};
			switch (ev.type) {
				case "BOT_MESSAGE": {
					const msg = getMessage(ev.msg);
					return <BotMessage msg={msg} id={ev.id} />;
				}
				case "USER_MESSAGE": {
					return <UserMessage msg={ev.msg} id={ev.id} />;
				}
			}
		}
		renderInteraction(action: IInteraction) {
			if (!action) {
				return null;
			}
			switch (action.type) {
				case "OPTIONS": {
					return (
						<OptionQuestion
							onChange={val => {
								action.onChange(val);
							}}
							options={action.options}
						/>
					);
				}
				case "TEXT": {
					return (
						<TextQuestion
							onChange={val => {
								action.onChange(val);
							}}
						/>
					);
				}
				case "RATING": {
					return (
						<RatingQuestion
							options={action.options}
							onChange={action.onChange}
						/>
					);
				}
			}
		}
		componentWillUpdate() {
			this.log();
		}
		componentDidUpdate(prevProps) {
			// Scroll to bottom if
			this.scroll(prevProps);
		}
		onHistoryChange = history => {
			// Do something with history;
			const getText = id=>{
				const doc = document.getElementById(id);
				return doc && doc.innerHTML;
			};
			const getLocalStorage = (key, defaultValue)=>{
				try {
					const val = localStorage.getItem(key);
					return val || defaultValue;
				}
				catch(e) {
					return defaultValue;
				}
			}
			const emp = {
				emp_no: getLocalStorage("EmployeeNo", getText("lblEmployeeNo")),
				emp_email: getLocalStorage("Email", getText("lblEmail")),
				// emp_mob: getText("lblMobile"),
				// emp_gender: getText("lblGender"),
				emp_name: getText("lblEmployeeName")
			}
			axios("/chat/api/logbot", {
				method: "POST",
				baseURL: "https://bot.sayint.ai/qa",
				data: JSON.stringify({
					history,
					emp
				}),
				headers: {
					"Content-Type": "application/json",
					Authorization: addToken()
				}
			});
			// alert(JSON.stringify(history))
		};
		scroll = (prevProps) => {
			const { scrollTop, oHeight, scrollHeight } = this.logPosition;
			// console.log(this.logPosition, 'scrollHeight === scrollTop + oHeight', scrollHeight === scrollTop + oHeight);
			if (scrollHeight >= scrollTop + oHeight) {
				// his.contentRef.scrollTop = this.contentRef.current.scrollHeight;
				// console.log("UPDATE SCROLL")
				this.contentRef.current.scrollTop = this.contentRef.current.scrollHeight;
			}
		};
		log = () => {
			const r = this.contentRef.current;
			this.logPosition = {
				cHeight: r.clientHeight,
				scrollHeight: r.scrollHeight,
				oHeight: r.offsetHeight,
				scrollTop: r.scrollTop
			};
		};
		onReset = () => {
			this.flow.reset(this.run);
		};

		render() {
			const { interaction } = this.state.flowState;
			// console.log("INTERACTION", interaction);
			return (
				<MuiThemeProvider theme={theme}>
					<Paper
						elevation={2}
						style={{
							display: "flex",
							position: "relative",
							flexDirection: "column",
							width: 560,
							maxWidth: "100vw",
							height: "calc(100vh - 64px)",
							// maxHeight: "calc(100vh - 50px)",
							margin: "auto"
						}}
					>
						<AppBar position="static">
							<Toolbar>
								<Typography
									style={{ color: "white", flexGrow: 1 }}
									variant={"headline"}
								>
									Smart Assistant
								</Typography>
								{/* <MuiThemeProvider theme={darkTheme}> */}
								{/* <TextField
				 				id="tl-timeout"
				 				label="API Timeout (s)"
								type="number"
								defaultValue={5}
				 				className={classNames(classes.textField, classes.dense)}
				 				margin="dense"
				 				variant="outlined"
			 				/> */}
								{/* </MuiThemeProvider> */}
								<Tooltip
									title="Reset to start"
									aria-label="Reset to start"
								>
									<IconButton
										onClick={this.onReset}
										color="inherit"
										aria-label="Menu"
									>
										<RefreshIcon />
									</IconButton>
								</Tooltip>
								<Tooltip title="Minimize" aria-label="Minimize">
									<IconButton
										onClick={this.props.onClose}
										color="inherit"
										aria-label="Menu"
									>
										<CloseIcon />
									</IconButton>
								</Tooltip>
								{/* <Tooltip title="Logout" aria-label="logout">
							<IconButton
								onClick={() => {
									(window as any).keycloak &&
									(window as any).keycloak.logout();
								}}
								color="inherit"
								aria-label="Menu"
							>
								<LogoutIcon />
							</IconButton>
							</Tooltip> */}
							</Toolbar>
						</AppBar>
						{/*marginBottom: 100*/}
						<div
							style={{
								display: "flex",
								flexDirection: "column",
								flex: "1 1 0",
								boxSizing: "border-box",
								overflow: "auto",
								padding: 20,
								transition: "all 0.5s",
								backgroundColor: "#F5F5F5",
								// marginBottom: 100,
								position: "relative"
							}}
							// onTransitionEnd={this.scroll}
							ref={this.contentRef}
						>
							<div style={{ flex: "1 1 auto" }} />
							{this.state.flowState.events.map((ev, i) => {
								// console.log(ev);
								return (
									<div className="enter" key={i}>
										{this.renderComponent(ev)}
									</div>
								);
							})}
							{this.state.botEnd && this.renderComponent({
								type: "BOT_MESSAGE",
								msg: "Thank you for using Smart Assistant :)"
							})}
						</div>
						{/*display: "flex"*/}
						<div
							style={{
								display: "flex",
								alignItems: "center",
								justifyContent: "center",
								bottom: 0,
								boxSizing: "border-box",
								minHeight: this.state.botEnd?0:100,
								height: this.state.botEnd?0:"auto",
								left: 0,
								width: "100%",
								padding: 10,
								// transition: "all 0.5s",
								backgroundColor: "#FFF"
							}}
						>
							{interaction && this.renderInteraction(interaction)}
							<LinearProgress color="primary" variant="query" />
						</div>
					</Paper>
				</MuiThemeProvider>
			);
		}
	}
);
