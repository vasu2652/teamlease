import _ from "lodash";
import { v4 } from "uuid";
import {
	executeAction,
	IAction,
	IEvent,
	IExecActionValue,
	IInteraction
} from "./action";
import { IFlowGraph, INode, INodeMap } from "./schema";
import { Timeline } from "./Timeline";

export { IEvent, IInteraction, IAction } from "./action";
export { IFlowGraph, INode, INodeMap } from "./schema";

interface IFlowPoint {
	path: string;
	// -1 for undetermined action to the given path!
	actionIndex: number;
	data: any;
}

export type IHistory = {
	point: IFlowPoint;
	events: IEvent[];
	// This is to automate flow!
	valReturned?: any;
	gotoValue?: any;
}[];

export interface IFlowState {
	events: IEvent[];
	interaction?: IInteraction;
}
type IOnChange = (state: IFlowState) => void;
export type IOnHistoryChange = (history: IHistory) => void;

export class FlatFlow {
	timeline: Timeline;
	private flow: IFlowGraph;
	private nodeMap: INodeMap;
	private history: IHistory = [];

	private state: IFlowState = { events: [] };
	private _onChange: IOnChange;
	private _onHistoryChange?: IOnHistoryChange;
	private _onEnd: any;
	private _onContinue: any;

	private status: "end" | "progress" = "progress";

	constructor({
		flow,
		nodeMap,
		onEnd,
		onContinue,
		onChange,
		onHistoryChange
	}: {
		flow: IFlowGraph;
		nodeMap: INodeMap;
		onEnd?: any;
		onContinue?: any;

		onChange: IOnChange;
		onHistoryChange?: (history: IHistory) => void;
	}) {
		this.flow = flow;
		this.nodeMap = nodeMap;
		this._onChange = onChange;
		this._onHistoryChange = onHistoryChange;
		this._onEnd = onEnd;
		this._onContinue = onContinue;

		this.timeline = new Timeline({
			flow,
			nodeMap
		});
	}

	onChange = (func: (state: IFlowState) => IFlowState) => {
		this.state = func(this.state);

		this._onChange(this.state);
	};
	onEnd() {
		this.status = "end";
		this._onEnd && this._onEnd();
	}
	onHistoryChange = () => {
		this._onHistoryChange && this._onHistoryChange(this.history);
	};

	reset = run => {
		this.history = [];
		this.state = { events: [] };
		this.timeline.telepathTo("");
		this._onChange(this.state);
		this._onContinue();
		this.status = "progress";
		run();
	};

	undo = run => {
		this.history.pop();
		this.history = [...this.history];

		this.state = {
			events: this.history.length
				? this.history[this.history.length - 1].events
				: []
		};

		run();
	};

	async run() {
		if (this.status === "end") {
			return;
		}
		let actionIndex = 0;
		let path = "";
		let lastPointValue: any;
		let gotoPointValue: any;

		// These three modifies after execution of action.
		let data = {};
		let valReturned: any;
		let gotoValue: any;
		let events;

		// The action that needs to take place should always depend on
		// history object. Taking out last one from history to proceed.
		if (this.history.length) {
			// From previous history deduce the action Index.
			const lastPoint = this.history[this.history.length - 1];
			actionIndex = lastPoint.point.actionIndex + 1;
			path = lastPoint.point.path;
			data = lastPoint.point.data;
			lastPointValue = lastPoint.valReturned;
			gotoPointValue = lastPoint.gotoValue;
		}
		// console.log("HERE", lastPointValue, gotoPointValue, this.history);

		if (gotoPointValue) {
			this.timeline.telepathTo(gotoPointValue);
		} else {
			this.timeline.telepathTo(path);
		}

		const stats = this.timeline.getStats();

		// console.log("STATS", {
		// 	history: this.history,
		// 	stats,
		// 	data,
		// 	path,
		// 	actionIndex
		// });
		let action: IAction;
		switch (stats.type) {
			case "FLOW": {
				// Decisive action should take place here!
				// Or the flow should depend on the previous action that took place.
				let nextNode: string;
				if (lastPointValue) {
					nextNode = lastPointValue;
					valReturned = undefined;
				} else {
					const edges = stats.flow.edges || [];
					if (edges.length === 1) {
						nextNode = edges[0].node;
					} else if (edges.length === 0) {
						this.onEnd();
						// this._onEnd && this._onEnd();
						return;
					} else {
						const name = v4();
						action = {
							type: "OPTIONS",
							name,
							options: edges.map(e => ({
								value: e.node,
								option: e.asOption || e.node
							})),
							question: stats.flow.asQuestion
						};

						const result = await this.executeAction(action, data);
						// console.log("ACTION RESULTS", action.type, result);
						events = result.events;
						valReturned = result.valReturned;
						gotoValue = result.gotoValue;
						const val = _.get(data, name);
						nextNode = val;

						// Do not leave trace of intermediate values!
						delete data[name];
					}
				}
				if (!gotoPointValue) {
					this.timeline.next(nextNode);
				}
				this.history.push({
					events,
					point: {
						actionIndex: -1,
						data,
						path: this.timeline.path
					},
					valReturned,
					gotoValue
				});
				break;
			}
			case "NODE": {
				const node = stats.node;
				if (node.actions.length > actionIndex) {
					// Execute this action.
					action = node.actions[actionIndex];
					const result = await this.executeAction(action, data);
					events = result.events;
					valReturned = result.valReturned;
					gotoValue = result.gotoValue;
					this.history.push({
						events,
						point: {
							actionIndex:
								gotoValue !== "true" ? actionIndex : -1,
							data,
							path: gotoValue !== "true" ? path : valReturned
						},
						valReturned:
							gotoValue === "true" ? undefined : valReturned,
						gotoValue:
							gotoValue !== "true" ? undefined : valReturned
					});
				} else {
					this.timeline.next();
					this.history.push({
						point: {
							actionIndex: -1,
							data,
							path: this.timeline.path
						},
						events: [],
						valReturned: lastPointValue
					});
				}
				break;
			}
			default: {
				return "NO STATS FOUND";
			}
		}
		// console.log("STATS END", {
		// 	history: this.history,
		// 	stats,
		// 	data,
		// 	path,
		// 	actionIndex
		// });
		this.onHistoryChange();
	}
	async executeAction(action: IAction, data: any) {
		// console.log(this.timeline.path);
		const generator: any = executeAction(
			action,
			data,
			false,
			this.timeline.path,
			this.timeline
		);
		// const done = false;
		let value: any;
		let events: IEvent[] = [];
		let valReturned: any;
		let gotoValue: any;
		while (true) {
			// let gen : any;
			// if (this.running) {
			//
			// }
			// else {
			// 	generator.throw();
			// 	break;
			// }
			// const gen = await generator.next(value);
			const gen = await generator.next(value);
			if (gen.done) {
				console.log("STUCK");
				const val: IExecActionValue = gen.value;
				if (val.valReturned) {
					valReturned = await val.valReturned;
				}
				if (val.gotoValue) {
					gotoValue = await val.gotoValue;
				}
				break;
			}
			const not = gen.value;
			if (not.type === "EVENT") {
				events = [...events, not.event];
				if (not.event.id) {
					const month=["Mar 2019","Feb 2019","Jan 2019","Dec 2018","Nov 2018","Oct 2018","Sept 2018","Aug 2018","Jul 2018","Jun 2018","May 2018","Apr 2018"];
					not.event.msg.forEach((element, index) => {
						not.event.msg = `<a href="/src/3IFDL_824983_PaySlip_03_2019.PDF" style="color:white" download class="ResponseTemplate-root-190" data-reactroot=""><div class="ResponseTemplate-marginRight-195"><img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPHBhdGggc3R5bGU9ImZpbGw6I0UyRTVFNzsiIGQ9Ik0xMjgsMGMtMTcuNiwwLTMyLDE0LjQtMzIsMzJ2NDQ4YzAsMTcuNiwxNC40LDMyLDMyLDMyaDMyMGMxNy42LDAsMzItMTQuNCwzMi0zMlYxMjhMMzUyLDBIMTI4eiIvPgo8cGF0aCBzdHlsZT0iZmlsbDojQjBCN0JEOyIgZD0iTTM4NCwxMjhoOTZMMzUyLDB2OTZDMzUyLDExMy42LDM2Ni40LDEyOCwzODQsMTI4eiIvPgo8cG9seWdvbiBzdHlsZT0iZmlsbDojQ0FEMUQ4OyIgcG9pbnRzPSI0ODAsMjI0IDM4NCwxMjggNDgwLDEyOCAiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0YxNTY0MjsiIGQ9Ik00MTYsNDE2YzAsOC44LTcuMiwxNi0xNiwxNkg0OGMtOC44LDAtMTYtNy4yLTE2LTE2VjI1NmMwLTguOCw3LjItMTYsMTYtMTZoMzUyYzguOCwwLDE2LDcuMiwxNiwxNiAgVjQxNnoiLz4KPGc+Cgk8cGF0aCBzdHlsZT0iZmlsbDojRkZGRkZGOyIgZD0iTTEwMS43NDQsMzAzLjE1MmMwLTQuMjI0LDMuMzI4LTguODMyLDguNjg4LTguODMyaDI5LjU1MmMxNi42NCwwLDMxLjYxNiwxMS4xMzYsMzEuNjE2LDMyLjQ4ICAgYzAsMjAuMjI0LTE0Ljk3NiwzMS40ODgtMzEuNjE2LDMxLjQ4OGgtMjEuMzZ2MTYuODk2YzAsNS42MzItMy41ODQsOC44MTYtOC4xOTIsOC44MTZjLTQuMjI0LDAtOC42ODgtMy4xODQtOC42ODgtOC44MTZWMzAzLjE1MnogICAgTTExOC42MjQsMzEwLjQzMnYzMS44NzJoMjEuMzZjOC41NzYsMCwxNS4zNi03LjU2OCwxNS4zNi0xNS41MDRjMC04Ljk0NC02Ljc4NC0xNi4zNjgtMTUuMzYtMTYuMzY4SDExOC42MjR6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojRkZGRkZGOyIgZD0iTTE5Ni42NTYsMzg0Yy00LjIyNCwwLTguODMyLTIuMzA0LTguODMyLTcuOTJ2LTcyLjY3MmMwLTQuNTkyLDQuNjA4LTcuOTM2LDguODMyLTcuOTM2aDI5LjI5NiAgIGM1OC40NjQsMCw1Ny4xODQsODguNTI4LDEuMTUyLDg4LjUyOEgxOTYuNjU2eiBNMjA0LjcyLDMxMS4wODhWMzY4LjRoMjEuMjMyYzM0LjU0NCwwLDM2LjA4LTU3LjMxMiwwLTU3LjMxMkgyMDQuNzJ6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojRkZGRkZGOyIgZD0iTTMwMy44NzIsMzEyLjExMnYyMC4zMzZoMzIuNjI0YzQuNjA4LDAsOS4yMTYsNC42MDgsOS4yMTYsOS4wNzJjMCw0LjIyNC00LjYwOCw3LjY4LTkuMjE2LDcuNjggICBoLTMyLjYyNHYyNi44NjRjMCw0LjQ4LTMuMTg0LDcuOTItNy42NjQsNy45MmMtNS42MzIsMC05LjA3Mi0zLjQ0LTkuMDcyLTcuOTJ2LTcyLjY3MmMwLTQuNTkyLDMuNDU2LTcuOTM2LDkuMDcyLTcuOTM2aDQ0LjkxMiAgIGM1LjYzMiwwLDguOTYsMy4zNDQsOC45Niw3LjkzNmMwLDQuMDk2LTMuMzI4LDguNzA0LTguOTYsOC43MDRoLTM3LjI0OFYzMTIuMTEyeiIvPgo8L2c+CjxwYXRoIHN0eWxlPSJmaWxsOiNDQUQxRDg7IiBkPSJNNDAwLDQzMkg5NnYxNmgzMDRjOC44LDAsMTYtNy4yLDE2LTE2di0xNkM0MTYsNDI0LjgsNDA4LjgsNDMyLDQwMCw0MzJ6Ii8+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" width="32"/></div><div>Download</div></a>`;
						
						this.onChange(state => ({
							events: [...state.events, not.event]
						}));
					});
					delete not.event.id;
				} else {
					this.onChange(state => ({
						events: [...state.events, not.event]
					}));
				}
			} else {
				// Interaction goes here!
				value = await this.interact(not.interaction);
			}
		}
		return { events, valReturned, gotoValue };
	}
	interact(interaction: IInteraction) {
		return new Promise(resolve => {
			this.onChange((state: IFlowState) => {
				return {
					...state,
					interaction: {
						...interaction,
						onChange: (val: any) => {
							resolve(val);
						}
					}
				};
			});
		});
	}
}
