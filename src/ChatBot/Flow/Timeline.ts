import _ from "lodash";
import { IFlowGraph, INode, INodeMap } from "./schema";

type IStats =
	| {
			type: "NODE";
			nodeid: string;
			node: INode;
	  }
	| { type: "FLOW"; flow: IFlowGraph };

interface IAtomicUnit {
	path: string;
	actionIndex: number;
}

export class Timeline {
	private _path: string;
	private flow: IFlowGraph;
	private nodeMap: INodeMap;

	constructor({
		flow,
		nodeMap,
		path
	}: {
		flow: IFlowGraph;
		nodeMap: INodeMap;
		path?: string;
	}) {
		this.flow = flow;
		this.nodeMap = nodeMap;

		this._path = path || this.flow.node;
	}
	get path() {
		return this._path;
	}
	getStats(): IStats {
		const pathArray = this._path.split(".");
		let nodeid: string;
		if (pathArray.length === 1) {
			// This is the start of flow!
			nodeid = this.flow.node;
		} else if (pathArray[pathArray.length - 1] === "") {
			// Intermediate step in flow!
			pathArray.shift();
			pathArray.pop();
			const flow = pathArray.reduce((agg, id) => {
				return _.find(agg.edges, e => e.node === id) as IFlowGraph;
			}, this.flow);
			return {
				type: "FLOW",
				flow
			};
		} else {
			pathArray.shift();
			// console.log("PATH ARRAY", pathArray);
			const flow = pathArray.reduce((agg, id) => {
				return _.find(agg.edges, e => e.node === id) as IFlowGraph;
			}, this.flow);
			if (!flow) {
				throw new Error(
					`Flow to specified path not present : ${this.path}`
				);
			}
			nodeid = pathArray[pathArray.length - 1];
		}
		return {
			type: "NODE",
			nodeid,
			node: this.nodeMap[nodeid]
		};
	}
	/**
	 * Pretty dangerous. Do not call this manually.
	 * @param path
	 */
	telepathTo(path?: string) {
		this._path = path || this.flow.node;
	}
	next(nodeid?: string) {
		const lChar = this._path.charAt(this._path.length - 1);
		if (!nodeid) {
			this._path = `${this._path}${lChar === "." ? "" : "."}`;
			return this._path;
		}
		this._path = `${this._path}${lChar === "." ? "" : "."}${nodeid}`;
		return this._path;
	}
	prev() {
		const pathArray = this._path.split(".");
		pathArray.pop();
		this._path = pathArray.join(".");
		return this._path;
	}
}
