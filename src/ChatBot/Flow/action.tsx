import axios from 'axios';
import _ from "lodash";
import { v4 } from 'uuid';

import RefreshIcon from "@material-ui/icons/Refresh";
import SaveIcon from "@material-ui/icons/SaveAlt";
import { Input, Typography } from "@material-ui/core";
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import LinearProgress from '@material-ui/core/LinearProgress';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import DownloadIcon from '@material-ui/icons/ArrowDownwardOutlined';
import React from 'react';
import ReactDOM from 'react-dom';
import ReactDOMServer from 'react-dom/server';
import {errorIcon, pdfIcon} from './pdf-icon';
const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  fullRow: {
    flex: '1 1 100%',
  },
  link: {
    outline: 'none !important',
    textDecoration: 'none !important',
  },
  linearColorPrimary: {
    backgroundColor: '#e2eerrorIcon2e2',
    flex: '1 1 100%',
    borderRadius: 6,
  },
  margin: {
    margin: theme.spacing.unit,
  },
  marginRight: {
    marginRight: theme.spacing.unit,
  },
  linearBarColorPrimary: {
    backgroundColor: '#fff',
  },
} as any);

// action generates events.
// Action may expect input from user or network request or anything.
// Hence events are async within action.
// Hence action returns a generator!

type IFunc = ((val: any) => any) | string | string[];

export type IAction =
    | {
    type: "BOT_MESSAGE";
    message: IFunc;
}
    | {
    type: "USER_MESSAGE";
    message: IFunc;
}
    | {
    type: "INPUT_TEXT";
    question?: IFunc;
    name: string;
}
    | {
    type: "OPTIONS";
    result?: string;
    question?: IFunc;
    options: {
        option: string;
        value: string;
    }[];
    name: string;
}
    | {
    type: "RATING";
    question?: IFunc;
    options: string[];
    name: string;
}
    | {
    type: "ASYNCFLOW";
    api: string;
}
    | {
    type: "ASYNC";
    action: (data: any) => Promise<any>;
};

export type IEvent =
    | {
    type: "LOADING";
}
    | {
    type: "BOT_START";
}
    | {
    type: "BOT_END";
}
    | {
    type: "BOT_MESSAGE";
    msg: string[] | string;
    id?: string;
}
    | {
    type: "USER_MESSAGE";
    msg: string[] | string;
    id?: string;
};
export type IInteraction =
    | {
    type: "TEXT";
    validation?: string;
    onChange: (val: any) => void;
}
    | {
    type: "OPTIONS";
    options: {
        option: string;
        value: string;
    }[];
    onChange: (val: any) => void;
}
    | {
    type: "RATING";
    options: string[];
    onChange: (val: any) => void;
};

interface IGenEvent {
    type: "EVENT";
    event: IEvent;
}

const genEvent = (event: IEvent): IGenEvent => ({
    type: "EVENT",
    event
});

interface IGenInteraction {
    type: "INTERACTION";
    interaction: IInteraction;
}

const genInteraction = (interaction: IInteraction): IGenInteraction => ({
    type: "INTERACTION",
    interaction
});

const utils = {
    genMessage(msg: IFunc, data: any): string[] {
        if (typeof msg === "string") {
            return [msg];
        } else if (_.isFunction(msg)) {
            return msg(data);
        }
        return msg;
    }
};

const pdfResponseTemplate = (filename) => {
    const image = 'https://orig00.deviantart.net/9979/f/2016/006/b/c/pdf_icon_svg_by_qubodup-d9n1mhy.png';
    const containerStyle = JSON.stringify({
        display: "flex",
        'justify-content': "space-between",
        "align-items": "center"
    }).replace('{', '').replace('}', '');
    return '';
    return `<div style=${containerStyle}>
                <div><img src="${image}" width="32" /></div>
                <div>${filename} - Loading</div>
            </div>`
};

export interface IExecActionValue {
    data: any;
    valReturned: any;
    gotoValue: any;
}

class ResponseTemplate extends React.Component<any, any> {
  constructor(props) {
    super(props);
  }
  retry = () => {

  }
  render() {

    const { url, loading, classes, href, filename, error, index, id, msgs, date } = this.props;
    // console.log(filename);
    const timeoutError = 'Request to TL servers timed out, try again';
    const showTryAgain = !_.isEmpty(msgs) && _.isArray(msgs) && (msgs[0] === timeoutError);
    // {
    //   !_.isEmpty(msgs) &&
    //   <div className={classes.fullRow}>{msgs[0]}</div>
    // }
    let image = pdfIcon;

    if (!showTryAgain && error === '1') {
      image = errorIcon;
    }

    if(!loading) {
      return (
        <div className={classes.root}>
        <div className={classes.marginRight}><img src={image} width="32" /></div>
          <div>{filename}</div>
          {
            error !== '1' &&
            <a href={href} download={filename} className={classes.link}>
            <Tooltip title="Click to Download" aria-label="Click to Download">
              <Fab

                classes={{
                  root: classes.margin
                }}
                color="secondary"
                size="small"
                aria-label={filename}>
                <SaveIcon />
              </Fab>
            </Tooltip>
            </a>
          }
          {
            showTryAgain &&
            <Tooltip title="Try Again" aria-label="Try Again">
              <Fab
                id={`${id}-${index}-retry`}
                classes={{
                  root: classes.margin
                }}
                onClick={this.retry}
                color="secondary"
                size="small"
                aria-label={filename}>
                <RefreshIcon />
              </Fab>
            </Tooltip>
          }
          {
            !_.isEmpty(msgs) &&
            <div id={v4()} style={{
              flex: '0 0 100%'
            }}>{msgs[0]}</div>
          }
        </div>
      )
    }
    return (
      <div className={classes.root}>
        <div className={classes.marginRight}><img src={image} width="32" /></div>
        <div>Loading</div>
        <LinearProgress
        classes={{
          colorPrimary: classes.linearColorPrimary,
          barColorPrimary: classes.linearBarColorPrimary,
        }}
      />
      </div>
    )
  }
}

const ResponseTemplateStyled = withStyles(styles)(ResponseTemplate);

const updateResponse = (res, id, i, uri, data, currentPath) => {
  try {

      const element = (document.getElementById(`${id}-${i}-content`) as any);
      const url = window.URL.createObjectURL(new Blob([(res as any).data], {type: "application/pdf"}));
      const extractHeader = (key, fallback = 'Unknown') => ((res as any).headers && (res as any).headers[key]) || fallback;
      const date = extractHeader('request-date');
      const filename = extractHeader('filename', date);
      const error = extractHeader('api-error', false as any);
      const msgs = JSON.parse(extractHeader('bot-message', '[""]' as any));

      const html = ReactDOMServer.renderToString(
        <ResponseTemplateStyled id={id} loading={false} href={url} filename={filename} error={error} index={i} msgs={msgs} date={date} apiEndPoint={uri} />
      );
      element.innerHTML = html;
      const retryButton = document.getElementById(`${id}-${i}-retry`);



      if ((retryButton as any) !== null) {
        (retryButton as any).addEventListener('click', () => {
          const html = ReactDOMServer.renderToString(<ResponseTemplateStyled loading={true} id={id} />);
          element.innerHTML = html;
          retryRequest(id, uri, i, data, currentPath);

        })
      }
 } catch (e) {
      // console.log(e);
      // console.log(e.message)
  }
}

const retryRequest = (id, url, index, data, currentPath) => {
    const promise = new Promise((resolve, reject) => {
        try {
          const element = document.querySelector('#tl-timeout');
          let timeout = 5;
          if (element !== null) {
            timeout = (element as any).value as any;
          }
            axios.get(url, {
               /** TODO Change to bot.sayint.ai before bundling */
                baseURL: 'https://bot.sayint.ai/qa',
                responseType: (url as string).includes('download') ? 'blob' : 'json',
                headers: {
                  Authorization: addToken()
                },
                params: {
                    chatContext: data,
                    currentPath: currentPath,
                    timeout,
                },
            }).then((res) => {
              window.setTimeout(() => {
                updateResponse(res, id, index, url, data, currentPath);
              }, 600);
              resolve(res);
            }).catch(e => {
                resolve({
                    "Error": true,
                    "ErrorMessage": e.message,
                });
            });
        } catch(e) {
            resolve({
                "Error": true,
                "ErrorMessage": e.message,
            });
        }
    });
    return promise;
};

export const addToken = () => {
  if ((window as any).keycloak) {
    return 'Bearer ' + (window as any).keycloak.token;
  }
  try {
    return 'Bearer ' + window.localStorage.getItem('RequestToken');
  } catch(e) {
    return 'ERROR'
  }
  return 'no auth';
}


export async function* executeAction(action: IAction,
                                     data: any,
                                     noInteraction = false,
                                     currentPath: string,
                                     timeline: any,) {
    let valReturned: any;
    let gotoValue: any;
    switch (action.type) {
        case "BOT_MESSAGE": {
            yield genEvent({
                type: "BOT_MESSAGE",
                msg: await utils.genMessage(action.message, data)
            });
            break;
        }
        case "USER_MESSAGE": {
            yield genEvent({
                type: "USER_MESSAGE",
                msg: await utils.genMessage(action.message, data)
            });
            break;
        }
        case "INPUT_TEXT": {
            if (action.question) {
                yield genEvent({
                    type: "BOT_MESSAGE",
                    msg: utils.genMessage(action.question, data)
                });
            }
            let msg;
            if (noInteraction) {
                msg = _.get(data, action.name);
            } else {
                msg = yield genInteraction({
                    type: "TEXT",
                    onChange: () => {
                    }
                });
                _.set(data, action.name, msg);
            }

            yield genEvent({
                type: "USER_MESSAGE",
                msg
            });
            break;
        }
        case "OPTIONS": {
            if (action.question) {
                yield genEvent({
                    type: "BOT_MESSAGE",
                    msg: utils.genMessage(action.question, data)
                });
            }
            let option_val;
            if (noInteraction) {
                option_val = _.get(data, action.name);
            } else {
                option_val = yield genInteraction({
                    type: "OPTIONS",
                    options: action.options,
                    onChange: () => {
                    }
                });
                _.set(data, action.name, option_val);
            }
            const selected_option = action.options.find(
                o => o.value === option_val
            );
            yield genEvent({
                type: "USER_MESSAGE",
                msg:
                action.result ||
                (selected_option && selected_option.option) ||
                option_val
            });
            break;
        }
        case "RATING": {
            if (action.question) {
                yield genEvent({
                    type: "BOT_MESSAGE",
                    msg: utils.genMessage(action.question, data)
                });
            }
            let rating_val;
            if (noInteraction) {
                rating_val = _.get(data, action.name);
            } else {
                rating_val = yield genInteraction({
                    type: "RATING",
                    options: action.options,
                    onChange: () => {
                    }
                });
                _.set(data, action.name, rating_val);
            }
            const ratings = rating_val
                .map(r => {
                    return `<div>${r.name} : ${r.value}</div>`;
                })
                .join("");
            yield genEvent({
                type: "USER_MESSAGE",
                msg: ratings
            });
            break;
        }
        case "ASYNCFLOW": {
            const api = [].concat((action.api as any));
            const id = v4();
            const resTemplates = api.filter( url => (url as string).includes('download') ).map(
                url => pdfResponseTemplate(url)
            );

            if(!_.isEmpty(resTemplates)) {
                yield genEvent({
                    type: "BOT_MESSAGE",
                    msg: resTemplates.map(() => ReactDOMServer.renderToString(<ResponseTemplateStyled loading={true} id={id} />)),
                    id: id
                });
            }


            let msgs = [];

            const makeRequest = (url, index) => {
                const promise = new Promise((resolve, reject) => {
                    try {
                      const element = document.querySelector('#tl-timeout');
                      let timeout = 5;
                      if (element !== null) {
                        timeout = (element as any).value as any;
                      }
                        axios.get(url, {
                            baseURL: 'https://bot.sayint.ai/qa',
                            responseType: (url as string).includes('download') ? 'blob' : 'json',
                            headers: {
                              Authorization: addToken()
                            },
                            params: {
                                chatContext: data,
                                currentPath: currentPath,
                                timeout,
                            },
                        }).then((res) => {
                          window.setTimeout(() => {
                            updateResponse(res, id, index, url, data, currentPath);
                          }, 2000);
                          if(!_.isEmpty((res as any).headers['bot-message'])) {
                            msgs = (msgs as any).concat(JSON.parse((res as any).headers['bot-message']));
                          }
                          resolve(res);
                        }).catch(e => {
                            resolve({
                                "Error": true,
                                "ErrorMessage": e.message,
                            });
                        });
                    } catch(e) {
                        resolve({
                            "Error": true,
                            "ErrorMessage": e.message,
                        });
                    }
                });
                return promise;
            };


            const d = await Promise.all(api.map((url, i) => makeRequest(url, i)));
            if(!_.isEmpty(msgs) && _.isEmpty(resTemplates)) {
                yield genEvent({
                    type: "BOT_MESSAGE",
                    msg: msgs,
                });
            }





            // if(!_.isEmpty(resTemplates)) {
            //     window.setTimeout(() => {
            //         for (let i = 0; i < d.length; i++) {
            //
            //         }
            //     }, 600);
            // }





            if (api.findIndex(i =>  (i as any).includes('download') ) > -1 ) {
                // (window as any).res = res[0];
                // const url = window.URL.createObjectURL(new Blob([res.data]));
                // const link = document.createElement('a');
                // link.href = url;
                // link.setAttribute('download', 'file.pdf');
                // document.body.appendChild(link);
                // link.click();
            } else {
              const status = d.find( (itm) => _.isObject((itm as any).headers) )
              valReturned = (status as any).headers['next-node'];
              gotoValue = (status as any).headers['next-is-goto'];
            }
            break;

        }
        case "ASYNC": {
            // console.log("ASYNC", data, action);
            valReturned = action.action(data);
            break;
        }
    }
    // Status of executed Action!
    const val: IExecActionValue = {
        data,
        valReturned,
        gotoValue
    };
    return val as any;
}
