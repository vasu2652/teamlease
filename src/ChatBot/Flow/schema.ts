import { IAction } from "./action";

export interface IFlowGraph {
	node: string;
	edges?: IFlowGraph[];
	asQuestion?: string;
	asOption?: string;
}

export interface INode {
	question_text?: string;
	option_text?: string;
	actions: IAction[];
}

export interface INodeMap {
	[id: string]: INode;
}
