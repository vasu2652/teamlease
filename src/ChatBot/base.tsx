import _ from "lodash";
import React from "react";
import {
	FlatFlow,
	IFlowGraph,
	IFlowState,
	IHistory,
	INodeMap,
	IOnHistoryChange
} from "./Flow/flatout";
import * as utils from "./utils";

interface IBaseProps {
	flowGraph: IFlowGraph;
	nodeMap: INodeMap;
}
interface IBaseState {
	botEnd: boolean;
	flowState: IFlowState;
	history: IHistory;
}
export abstract class ChatBotBase<IProps, IState> extends React.Component<
	IBaseProps & IProps,
	IBaseState & IState
> {
	// Delay 0
	delay = utils.DelayBuffer(400);
	protected flow: FlatFlow;

	// tslint:disable-next-line: member-ordering
	abstract onHistoryChange?: IOnHistoryChange;

	constructor(props: IBaseProps & IProps) {
		super(props);
		this.state = {
			flowState: {
				events: []
			},
			botEnd: false,
			history: []
		} as any;
		this.flow = new FlatFlow({
			flow: props.flowGraph,
			nodeMap: props.nodeMap,
			onEnd: () => {
				this.setState({ botEnd: true } as any);
			},
			onContinue: ()=>{
				this.setState({botEnd: false} as any);
			},
			onChange: state => {
				this.setState({
					flowState: state as any
				});
				// this.delay(() => {
				// 	this.setState({
				// 		flowState: state as any
				// 	});
				// });
			},
			onHistoryChange: history => {
				this.onHistoryChange && this.onHistoryChange(history);
			}
		});
	}
}
