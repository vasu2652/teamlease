import _ from "lodash";
import { IAction } from "./Flow/action";
import { IFlowGraph, INodeMap } from "./Flow/schema";
export interface ITemplate {
	graphs: {
		[id: string]: {
			id: string;
			graph: IFlowGraph;
		};
	};
	nodes: {
		[id: string]: {
			id: string;
			flow: string | null;
			texts?: {
				option?: string;
			};
			actions: string[];
		};
	};
	actions: {
		[id: string]: {
			id: string;
			type: IAction["type"];
			params: {
				name: string;
				[id: string]: any;
			};
		};
	};
}

export class Template {
	template: ITemplate;
	constructor(template: ITemplate) {
		this.template = template;
	}
	getConfig(flowid: string) {
		return {
			flow: this.getDeNormalizedFlow(flowid),
			nodeMap: this.getNodeMap()
		};
	}
	getDeNormalizedFlow(id: string): IFlowGraph {
		
		const graph = this.template.graphs[id].graph;
		// Normalize graph to add flow if it is present!
		const deNormalize = (f: IFlowGraph): IFlowGraph => {
			if (!f) {
				return f;
			}
			if(!this.template.nodes[f.node]) {
				console.log("UNKNWON", f.node)
			}
			
			const { flow } = this.template.nodes[f.node];
			if (flow !== null) {
				// Append flow.
				const fGraph = this.template.graphs[flow].graph;
				return {
					...f,
					node: fGraph.node,
					edges: fGraph.edges,
					asOption: _.get(
						this.getNode(fGraph.node),
						"texts.option_text",
						undefined
					)
				};
			}
			return {
				...f,
				edges: (f.edges || []).map(g => deNormalize(g)),
				asOption: _.get(this.getNode(f.node), "texts.option_text")
			};
		};
		return deNormalize(graph);
	}
	getNodeMap() {
		const nodeMap: INodeMap = {};
		Object.keys(this.template.nodes).forEach(n => {
			const node = this.template.nodes[n];
			if (node.flow) {
				return;
			}
			nodeMap[n] = {
				actions: node.actions.map(a => this.getAction(a))
			};
		});
		return nodeMap;
	}
	getNode(id) {
		const node = this.template.nodes[id];
		return node;
	}
	getAction(aid: string): IAction {
		if(!this.template.actions[aid]) {
			// console.log(aid);
		}
		const {
			id,
			type,
			params: { texts, ...other }
		} = this.template.actions[aid];
		return {
			type,
			...texts,
			...(other as any)
		};
	}
}
