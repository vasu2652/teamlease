import { Input, LinearProgress, Typography } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import _ from "lodash";
import React from "react";
const sayintlogo = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMiIgaGVpZ2h0PSIzOSIgdmlld0JveD0iMCAwIDMyIDM5Ij4KICAgIDxwYXRoIGZpbGw9IiM2Njc3RTciIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTIwLjUzMiAzMy45NjZsLTguMjkgNC44MXMtMS4xMzcuMTYtLjQ1LTEuMDFjLjY4Mi0xLjE2NCAxLjQxLTIuNzE3LTEuNDQzLTQuMzg3bC04LjAzLTQuNjM2QTQuNjQgNC42NCAwIDAgMSAwIDI0LjcyNlYxMS42NjdBNC42NCA0LjY0IDAgMCAxIDIuMzIgNy42NWwxMS4zMDktNi41M2E0LjYzOCA0LjYzOCAwIDAgMSA0LjYzOSAwbDExLjMwOSA2LjUzYTQuNjQgNC42NCAwIDAgMSAyLjMyIDQuMDE3djEzLjA1OWE0LjY0IDQuNjQgMCAwIDEtMi4zMiA0LjAxN2wtOS4wNDUgNS4yMjN6TTQuNzI5IDE0LjI5OGwxOS45OCAxMS42OTZjNC4wMy0yLjc2My0yLjEzNS02LjM0Ny0yLjEzNS02LjM0N3MtNi45Ny00LjA0Ny05LjE1OC01LjMyYy0yLjE5LTEuMjctLjQ3NS0yLjIzMi0uNDc1LTIuMjMycy0uNzk3LjQ1NyAxLjAyMy0uNTk0YzEuODItMS4wNSAzLjM4Ny0uMTc1IDMuMzg3LS4xNzVsMy4zNTMgMS45MzFjMS4yMjguNzEgMS40NDEgMS44NCAxLjQ0MSAxLjg0bC4yNTQgMS40MDljLjkwMyAyLjY2MiA0LjM4OSAyLjI0NyA0LjM4OSAyLjI0N1YxNS4wNWMwLTMuMjA2LTIuMTk1LTQuNjUtMi4xOTUtNC42NXMtMi44NC0xLjMwNC01LjU0Ni0zLjI3MWMtMi43MDctMS45NjctNS4wMTMtLjc0NS01LjAxMy0uNzQ1cy01LjE5NiAyLjk4Mi03LjE3NiA0LjEzM2MtMS45ODEgMS4xNS0yLjEzIDMuNzgtMi4xMyAzLjc4em0xLjEwOCAxMC4wMjFhMS45NDYgMS45NDYgMCAxIDAgMC0zLjg5MSAxLjk0NiAxLjk0NiAwIDAgMCAwIDMuODkxem00Ljg2MyAyLjkxOGExLjk0NiAxLjk0NiAwIDEgMCAwLTMuODkgMS45NDYgMS45NDYgMCAwIDAgMCAzLjg5em00Ljg2NCAyLjkxOWExLjk0NiAxLjk0NiAwIDEgMCAwLTMuODkxIDEuOTQ2IDEuOTQ2IDAgMCAwIDAgMy44OXoiLz4KPC9zdmc+Cg==';

const tllogo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKoAAACqCAYAAAA9dtSCAAAACXBIWXMAAAsTAAALEwEAmpwYAAAgAElEQVR4nO1dd1xUxxa+u4Aae8EIIiiCvZcUE1PfM9HYNcQuCogF0RRjTIyxJ7HEEhPLiw2RrqCCXVGaLXZsqNhQUXpbyrb75szO4GVdcBfL7MX7x/fbZffey9mZb86cc2bmHI7neU6CBHMHcwEkSDAGzAWQIMEYMBdAggRjwFwACRKMAXMBJEgwBswFkCDBGDAXQIIEY8BcAAkSjAFzASRIMAbMBZAgwRgwF0CCBGPAXICKAq2W51QarRzBkkKt0cpYy1VRwFwAsUOj1cqAlOi9QVIiAmPyApFZyypmMBdAzFBrtRZARHifV6iSRSekNI25nvp+7PXUD45eS3nn7N1Ma3qtFl2LSC1nLbNYwVwAsQJpSQt4Tc4qeGNqwFmvN6eGneWG+aoQeAQtN9SXtxzll9N61p6I9dGJ/1WptXCfHJFbMgfKAeYCiBGUpBtjb3Wq7BZ4hXPx4WWj/TSceyBfAm4BvGw4Iu2Xm/lmMyJ8Eh7lVIP7JNvVdDAXQGxAJMPT99qjNz/khmzJlbn68xbjgtSImNqniIogcw/U4O+HbuEbeofGXknOqQH3ayTNahKYCyAmaAhJY2+kNkbTe7JsLNKYHoFqQwTVh4VnkAoRm2//4+6gQrUGnieTHCzjwVwAsYCQykKp0XIdf97jw43cysvHIfIZQVIKS89gNeeymf8p5MIAeKaamBASng3mAogFdMoHz54b7Zcn8wjiS5vuS4PcA5kAo/34Fj9G7FPqtKpc0qrGgbkAYgGJlXLTAs+OB3vTUmeXGk1SAi0yFXjO1S8nJiGlETyPDgAJZYO5AGIBcn7wNP3u/AN/wbRv4WHatP9k+g/ScEN8+MW7r/wHnqdSa6Tp3wgwF0AsoMH6br8e3MaN2Eo9/XITdcm+q9hOlYhqHJgLIBYUa9R5+/1eiEaNuNwPnicR1TgwF0As0BAbte/yqPnccF/QqOUiKrpPCzbuxqjEzvA8lVqyUY0BcwHEAkQo3WpUVGIPbtgWINzTK1HPAAT/YbWq1oSQm/czFG/A86TAv3FgLoBYQAmVlKGoVNMj6CJeHgXimaJNIeiPSN57SeQi/Ewpjmo0mAsgJtAA/aztF4ZxX/nwVp7BKmNjqbCCJXML5CuP8b8feyPVBp6jkdb8jQZzAcQEEpyX5SvVXLf5B9bCkmil8cHKZ2lWOXK8KoFt6rJZ+8uOeOxESfFT08BcALFBq8Xb9WTpiiLZO/P2/48btIm3GBughQUABCWCSgiwZeWj/Hjuy035M7ZfGALPkJZOTQdzAcQIYq/K03ILuW+Dzo2uMTHkDpgCMtiHqtuPiiEbugV5+D58s592R26Ou9VRy+s2W7OWX4xgLoCYgUwB6mBVXXbgWv/eK6L+7LMyemffldERvZZHhQxbd2z29jNJXRVFat31ZiCzWMFcADEDiAebqClhSwMsFqgkm/S5wFyAigBKWOEJVMFJVImgLwDMBaioUKvVMgQ5gYUA+DONRsNcRjGBuQBiByEkENASka88jhLcb0kgJ1EFCXpgLoDYAJqQEpPXO8uvVCq5vLw8Lj8/v3JcbGyDvXv2tDywf38nhA8RPkZ4/+CBA+13R0Q0S0hIqFtUVGQJ1+trVyC8SqUC4ksLAgTMBRADQMsBOYUaE/3N5eTkyOPi4hwC/P37fff1N7MG9R/g36Ft2+Nvd+ly166BTY517TrK+nXqahF4Au2bdetp6tWqXdi8qVMGui6ha6dOh9zHjF07d/bsCYjIbycnJ1dFJC3+34S0Fq+7qcBcAHMGmdYttWSdH8h59erVGsGBgZ97ursvQyQ7aWdjk4cIySNoG77ZgHe0d+CbNLLnmzVtyjd3cgJoETQC8M2dnXinxk34Jnb2+NoG9awxkYHEbVu1ujN4wMCAebPnuB47dsyWkhZkIObFa6llmQtgjiDODtaeoE3T09Mr/2/tus/69+m7ARHxERATkUsrIKQaQeXs6KhCBNQgaIGIeoDP1c5NHPG1BEp0TxF6RiF6XwREburQmEeE18L/sK3/Zs7Afv23rV2zpm9iYmJlIg+2ibWv2a4r5gKYE4TTO9ibJ44ft//+2+9+bt28xXXQeEAgRDRMTEQuSkp9QsI1KgEZi7UsvDo0tKNmAGhhHpkI+PPGdo14RwcHrIVbOjcratOiZUELJ2cN/E9kKvDOjZskzJg+feqtW7dqE8LKCWGZt5tE1FdH0GINCrbg4cOH2w/o228T0pp5aDoGImkxOR0xOQ1qSyAumtI1Tk2a8A62T8iIoG7bslV6lw4dr7Zv3Sbmg27v7fb28gqeNGFCyFRv7+DePXuFo++j0ffxaECkIPKqhCSGgYFIq0Y2LbZ10f96PH3atBm3b9+uQeTFIS/WbSgRtbzkQ5rmWUuWYO+BVkLvZUDQI5GRHdFUux0RVI2mXWpflqo5MTnRdA2aEEwB0HyOjRxSPnzv/X1eEyfOCwoM7L9/3762Dx8+rJObm2uZlZWFowLwv6kmLCgo4DIzM+Fzi3v37tXct3dvy62+vn0mT5r0c59evXYhgiZR0oNZAIQFmxa9vzdr5kxXdJ+cDLYK7XAxF+AlkVRWFkmpF4/e48D74UOHmg7s398XEUBFCKoh2lOfnFh7YvIizQmEAc2HNOFVjzFjf0fe/6dXLl+uBeQrZUqWQVY/QdyUxl4N2ptgfiDyVosID3/vh2nfz3m7c5czSMNrQdMiompB1rc6d4kJCQ7Gx1oqsnZlLsCLBuxsAsRcT32jSPW0hhFoUQ5Nn9XcXMfMRWTLAY+dTO/q0gjawtlZhWxNnea0d0hzdx2zFmnN7o8ePbISEpPYjyYH8ckAoitZEG2wEH6nUCjk27dt6zJi6LClyH69b2Ndn29kYwtOV4Gnu/ucBw8eWMG1EM6qaLYrcwFeJJRqDdYmv+6+0nZqwNk28J5mzhNoUVlhYSG3cvnyfoh0N6GzjdGgTezttXVr1uLf6tT56ozvp3tfvHixPnkuDV1Zvox4J5VbGCaD/4HMiRq///rbmPZt2pxEml0Dmr1z+w5xYaGhLXkdWS0rkinAXIAXBXr47vc9V956d+FBT+F3Qi166dIlm75f9A6oh0iHpm8t8d4NOUhgD6oQgTVgH3Zq1z5++R/LRqSlpb1BCUo05ivVXsTxKyYt0uYWvy1c2BfJF1OrajUeDby8ubNnD4XBiL63qChxV+YCvAgoydn41ZE3OjeYtG1LclZBVR2ZdB2L3suh41YsWzbUwc4uFWw7mMZLcZKAwBDrVIMNiuzBewvnz3fPzs6uxPPmE3gnmhYIi2eRlJQU2ZJFi/t3aNP2shx1q9uYMSuSk5PhWhnSrqK3W5kL8Lyg2+guPch2qOceeDb033sd6Oe0gy5fvlynZ48e/mBbIhJCKKk0OxR7+RDzRJop33uS14KEhIQahBhyQgzmv1kIahpQwiKTwGrK5MnetatX1yInL+bmzZs47gpmCWtZnwfMBXge0CPMV5JzqtZwD4wf97/j3vA3IiloPNxxgQEB7Vs1b54A2vEZWhRsUTXETbt27HRoT8Tu1uT/mCVB9SHYj4B/d1xsrG33bt0OIqfr1tEjR5xxuxDzR4xgLkD5Owa/yrMLVJytd+h+xymhh9MVSvyZioRo/LZubVe3Vq1U0JA4pORgUIvCMig4SxBuyp3i5TUZ2aE6B0nnHInKxqMmAa8LhXHIrnZDM8n5jevXvwXfi1WzMhegfJ2BX2FHPTdgVcxv3Fc+qt0XHjjijiCef2ZGhgzZa8eQTQpOkbKsqR5CU8hjPhERHt6KdLbo45GgWQlhudiYmBbtWrUOWrt69Ufwtxh/G3MBygPqPM3bGe/Cfb6OH/xXzA+4c+BsEtEYwUFBn9SqVh1pUmetIa/euYkj3s0EJsHwoUMXwZY9uE+Mmz1Ac5L8rfAbcN0rahbR34OcwTfGjxs3ceXy5V3F+DuZC2Aq6CG58PMPmnHDtuTX9w69fictD3v5uDiZSoW1CCLqGIgtQojJwFSvhJWlujVrZf+xZIkLvlekqzqwAge1rmCWeZCZb5GUobDSaHS5B2gMmSw6AGQL583veOb0abpPQDRkZS6AKdAQh+bG41wLu292REI+/JnbLgyFz2j6Rurp7wgLa1+vVi116xYtQHMq6W4m5FBpIDzVslmzK3siItqRDrMUm4YBUAcPDVTLMRtPTH1jQsipqhO3xX+8+PCK2BspDUmbyckr7GfAS8ZFRUWi+63MBTCtY3hMxoEror4HkiJteuJuukJGOkLYebL8/Hzuv598+idM/7ByQ3bXw/vsIS4uy27euFEHrhezJ0xJ2Gvpkd+4ARt4C7dANTc2QCODBBijtt5ZfjCBrs4VzxRiPZfFXABjQRs7/Nz99twI31wEfkbweZwiR6WXIod2xOPHjy0W/fbboOnTpq2YMX36H7NnzXKPi41tBJs9eF3YSXRTPQW1Qe+k5lWv7hn0AGoDyNwDlZx7oMbKM6gISgvJR/ndPnYztULUCmAugDGgycky85Wc47Rde5C24K0nbz91L11BpzUD92g5bSm1R8EeFZN9ZgjU/tx/7r4tN3RLpsW4klVa5B5BSs7Vj7f7OuxAWl4Rbj8xV2BhLoCRnYI15tyd8SMg2zNo05kh5yfAZ2WlFqcxRT2ImqAUVKPG38+qYTkuKJlzwxq1RApMnBUbtVfv5VEzyD2iNXOYC2BCh1SzGhtwGXfCWP97JxPT6sLnr2tdUbrgAfWqnL7fFc65+huqIqjFmnaIT+Hfh653Je0lShOAuQDPAtWmHhtOeGFtOsqP/2xJ5Er4DDTE65x4jNrmC3bGD4BsgrgyoIEEwsjB4utNCIm79ijHkrSb6AY3cwHKAm3Qi0lZNazcAq9j7TBsC78m8kY3+Px1zzNKIxw5hSqu/czdYaTs5VNktYTM2EN8+NFr42aItd2YC1AWqMaY7POvJ1QSgbKOVScEX7yVklscG2QtI2vQ9OpHrj22reoZfBPbqh5PZcDWmQAjtmZvP5OEN1aLzQRgLkBpoB5qpqKIazJt50mYvkBj9FgSuQR3kFZXTkfCkyXlRXuudEcDOt9y3NN1WrGmRe331rz9wQUqUofVDGQ3FswFKA10elq898qHoE0rTwjBNe//OpjQAz7Xj52+7qC1Wr/2OzMBagtYeBoyAaAY2xZ+YfjlL8TWhswFMAStDrgR319wcDWulAdawtU/49iNVGv4XKoootdmOnvVAmLNXX/Z6wshPH17FZsEY/x5h293xMFMpbuPvezGgLkAhkBJeOVBVnUr98A7uKFRA9t+u/NQTgHOxSSVDzcAGqo7dy+zbpWxATfBXNK3V3ENV+SQzt0ZP5LcIwqtylwAQ6BB/MW7r/Qk034RNO7gldFL8fcayT4tDdRe/X33lc9gPwQuaVnSXsXEtXALuHQxKQufAxODU8pcAENQEyIOXBm1CmKnlccHFwJRZ4ZeHAafS0QtGzQfwEcLD66EdpPr1W21BPsVKYCJm095kPY0e63KXAADjYxHd1a+UmY9NewcTPlWEMge4sMHH7/zDm5YqSJzmaCmE9KYtap6BCXol8PE5oCrP2//3Y4zGYqi4rwHrOUuC8wF0AeN70VefdyUG+2XL/cI0mkCt4Dc44lptuQas5+qWIMO5nmhFwdygzdB0F/zVARgKI4AiKIcO3MBnmpgklXvt4jLA6AhkTZVglat7R2ahDzVavCdGDc5v2oQDSlHMxPXdkbEDlh6RoNeLdCqatCqjt/t3F2gxHWwzNpBZS6APtDIxvbn8NWxc8C+gr2V3JgARNTt59E0JdpzTSxAZ6eA43c6oEGvkpdcCNDi2WrkVnXomaS2wuvNES/0YYJkCDhTHo9Hqe5EpzE2EDn/gzXqe78eCoT4qeW4oEKiUWNI7E9m7vaUOYFu3Ok6e+9q/b0AeBvgMF++74qo+XCtuhxOqiCxm3Ar5Qsn/At5CAhLEx8847oyCVu8yUKh5Op5bYsBTYpGfREh6gFCVFEepWAFqiX9j99ujrSqQqhVsVM1NoCv4bXtcnJWAU0Fb9RsBRvPS9uYrtffL2T2e+4HgMCUpPHx8TVX//334JHDhy8Z1H/ApgF9+m30dPdYsnnTpkEJCQk1SXY5WWnC089zCpSVkMd/EwiKd6rriHpQIqrpIOv5WKt2+WXvWqxVPZ6EqywhzopMrM1xt9+D640JVZF+wsmPUb/WXv/PPz3/t27dVPT6/bq1a6du8fH5LCkpqRa5vtT+fmVEpcc57ty5U3ni+PGzHBrapdapURPydeIkswCaidmugU2a68iRMx/cv19ZeG+J55EflJxdUBON8nQY7bhRdUQ9LBG1fBDYqu254b5qoValpwD6roz+nVxb5vRPp/UL5841HOri8re9bcNM6N+6NWvhtJzwimsONHHM+HLQoFXnz559k/T3c5kD5b6Rqv0jhw836tqx00kgqLOjIy1RoxQUW8DpwyHhA5wCbeHkdPzE8eO69Xq9YyH0zH7YmaRGaJTnwfo+mp4oUU89WZ+WiGpSX+mA2xbZqjuJVlUL1//reoeegQgBaV+DGpCS7e9Vqz5DSikF+hP1Ky1PVKK/ISM25J5FyupxUEDAZ+T+cofAynUT1YaQKQ4JdBFSdbds1kxZagKyJxmbi+wb2sH705cvXaojfFYJop5NckBGvoIQVU2IehUR1aqshpRQOmicdO6O+P4Q9kNTvqaE9z/Kr/DglUfN4BpD3j8l6bKlS/vUrl4D9zPqz7L6HPpbCXUHGtStVxQcGPhcZDX5BhqfKyoq4v7z8SdbGjZogLSkc1EZBC0BuNbG2pof0LefDxQY4wVTOSXqjrP37dF0pCOqu87grzpxW1pShgKnUBTD2rS5gbbZvXRFleqTtiUIN6xYjtMtqS7YdWkUIXWJ6Z+SNDIy0hGZcqlQW6uMDN0lUyc5OuIUn0i7Jp86edKePM/k/jP5B9OEDX6+Wz+GlDlQC8lYkhaT1dlZDfZMgJ9fd+EzBVM/0qhbFOQIsMZSdwRFGXTqbkvSkGYb7zNn0M3mvVdELQK71IKEqqid2mfZ0TU6Ij2xU6liglwIn3zw4UY7Gxu+mYE0SWUB0ipBIrovPu/5D3muyX6GyT+Wbnjo2eOzTfDPjR1Z+oLb1K/PuwwatE7XMLrpgC6NoinIFk1FuWT5VIunKeSZBp2615MQ2qyX+8wVdEvfhujEDwhRtcWrVMi8sp4SGpOts1OLY9XUeTp54qR9IxvbbNTffGmp5MsALiTX2K5R1qX4S1SrmqRsTBuRRGU/uP/Asm3LVjeg4hw4SaYSFe6Be9u0aHkhJSWlOH4nCE9VrzclNAUaD6YnSxyY3sKP8zn1jY6oGmn3VDlA2/demqJS9QkhN+hmFdTGWjAFkEnw+H5mPg4rUVOBpq5cMG/eSPDmmzd1Li1b97NMPjXcv3zZMl12GxPztJo2Ip+MLjtkc2SVc3RhT9GxkT0QNSc1NbW4uoiAqFbWU8MSKFHp1NTzjyO+ugEjadRyEVX3ituu69z9PpBxhsRUteSEr2rb6SScr4qaYZSoriNHLQAv31B2RCOJqoICH/Nmz1lIiGqSsikXUc+cOdMYefq5oM7LS1TIAo20cj4iagMBUeH/yBBROaRRjxKiqnUhlAC+jnfo5UyF0lJ3veEiYhLKBt3L6+nzrwfMUpZP7FQNLKeGnkn6nFxnQfocv3q6e6x5s87zE3Xh/PnLXgVRMTlOnz5tg4ia8VwaFU39rZo3z0x5/NiaEhX/D9KQff+M9iN2lEoQQlHtjX/YmlwnOVTlAG035Jh2IkSlfgA2ryb6nnbFRCKev0CjLnpejQoONDIhfn/pRKUGNtTzfLtLl3iolFxOG1UN1ZS7dOj4b3Z2dolnC3ZP/UwaUiUMoXhuPqXLOSVN/+UCtT3vpitqVJ0Y8hCHqdyJH4Dad9jauGmGiPrrggWepSVGNsVGnT933hhC1Jdno+IfSjz0USNGLIO04uURHO6BImPeXl6LhY1BGoikPb80iASm1SQ7nRpsqrfm7t9JrhXVuXRzQXFmRIWSqz15exSYVGBeUaIO/9/xhXpExRp4T8TuVoioRVCmvRyzKKxewXJ6/uGDh5oJn/vSiEr/gZ/v1vZIlStbOjfTOjmYJLi2hbMzFLvNRz++nb7QdGo6ePlRc260X6HcQ7d6QjPVyccGZJ29k2GnGzRS4L88oHHSnsujfKl5VaxR1x37U0hU6jeQBZ6dsAppakgSrof460fdu4cV6SoKvpI4Ko6lws6ZIS4ui0CdI1vT2JUpbctmzYpqV6/Be7i5zdU1WsklNWqrZuYWWlpP3n5ZuIJCj/qO33RqPG5MafovF2h4z2VN3HxqXgmIukJIVABVJCFBQV3q1apViPoQFJOxJh/sA+CRfZu/a+fOTsLnvVSiEnLpdk3dvm35VufOO2EaR8KrnB0dSx1pMKrgGiB2/z59t+fm5tLd+noDAb9iAnabt3+jcFsaPT7h8N3OWEURXn6VNlE/D1FXx403QNQSGlVAVtwnv8z8ecIbVpWgv9WkjmypSgn4gGZcDaSn/2bKFE/CnVez1q9P1luJiVY9e/TYAh4dcpCgCC4uJy7YSQOlxbXI8dKCJh3Yr/+mnJycyoSkBqdu2ki/hl8eDo1nIdiVjreooekq4MQdXOBL8v7LT9QNMYku+kRFNurvhohKl1JzkPM7Y/r0yai/Nfa2DaGvtXo7p3Ro6qQBh7lm1WrqyRMnTaAzcXkVy3P9YLqBFuyXlcuXD+rYrt2/UNSB7Esshm39NzVI855c/scf/Wj+/LJ2QFHyHbiU7IA0qoIupRYn+xqxle86e996LAPZFMy688UEeoAy+N97vWkstVijrtGlptQnKsWT0p3+Xd9/990jsP8UZlRYTof9x/AK0QE0c2rfe/udw3t27+6CPPxnnu54qUSlZKV7U5GmlCHBWq3+++/RsNsbYRp6Pyo6Orp5gc6INkpgOnrzlWqu6fe7DsB0LzhBqcVadeiWfL9jt1sIiS3BZKJ+SohavEzt5XfGrSyiCo8dKRQKbueOHW1n/vjjt/1791njMmjw5oF9+69dMG/+1wf2729DQ48vol7CC/vxRPiyBHrmGZuSjalzlLx8TuHcqJaC7HTYZkW2a5df9v4tPGrBmgBiASVq0Kl7PShRsXmFTKqws0m9hO1fRn8/84iJVlfbyjzOTBn6ARDMFZxILJddQgPTF+5lNrQYG5CBnSnhUV+dVlXAoTW4TtKqxoPaqEijDqY2qoWuPdWhp+7io9PGbqWE1UqN7nlyClLl+oWGDpk3WmkQbqB458kGCqFThRPTdp29dyO+XitpVWNBifpz2MXRmKieQUrYSYXI+vji/aya8J25xaiZC1B2g+qmn4URlz8i078wKx3VqjyyVbsLr5fwzHbFRP1yZfT3uiQfwTh3gvXUsCPZBSX3o5oLmAtQFmiKmfS8Is7hm7CTpESNMNkX3vBb32vb0eTsArNsYHME1ahfrY1bBUTF2RKRfTpwRRSOoZYnEcXLBnMBntmoZO1/4c74UfpOVfFq1RAf/hu/M3Szitk1sjmBmlSIjFz72ft2QU6qSuODi6BtF+2+MljY5uYE5gI8C9RWOp+UWdnCLfCSMBltCQdrxNaUg5eSG8O1kmNVOug+3ozcQsvaE0OuwBI1dqRGbVVEXnnkYK7tx1wAY0DP+rhvOOEOU5WFXm566li1nh6+i5xNl5JUlN6WmKgnb6U1Qg6UbjEFmVTOP4RHFiiLM9kwl1MfzAUwBoIyk1ZW7oEX9LUqNgEg2e9XPvxkn1Nfkw6RTAADoA7nXweu9QaTCaedR9P+1K2nvyPfm2W7MRfAWAgK934FRyYsn65Qpzv38+XmAt9jt/E+AKUZ2lqsQQewy6qYhdCOVpDgd5QfTPvO5Huzm/YBzAUwFnTDb4aiiLOfGnZIFwHQK08DWtYtgK82Pvj8haTMGqThzSoeaA5tmFuo4uy+2xkNm6ahHZ2+37XT3JP5MhfAFNDRvibyRndkk2rkhivUqWBx4O25+33T86SkakJoSPsdv5nWFDlRugp/w335xfuu9ifta7YzEHMBTAVN9Pv2nH3rSNEvlZ4JoAthIXt16Jq47829A14lqH06c9sFUls2kK85IeTiw6x8sz/Zy1wAU0Edq9gbqQ0sxvgnGXKsirOrjPDVLN137WO43lxtr1cFMqXLIX7acdaefbBQAtrU7Z/jXqR9zHowMxegPKCNOiv04le6OvVP1/2kq1YNvEOjHufgLYYyzWtsAtCBGn7+QUtE0EJwPK3cAq7F38+qDp+b29q+PpgLUB5Q7VCk1kAW5a2wkdqACaC1INmUfWJvdYX7VK+pVoXVKJogbcCqmF9gNQq06bhNp8bAZ+auTQHMBSgvqAlw7GaqrdzV/z45rVqylhLZte6x8SSOrZa2GVhsoEU9hAUeyko6RtvqdmpetWrjg69yo/34Jt/siMvMV5JiaOx/07PAXIDngfqJc9APin5VGh+sFkYBiom6+RTOH1ARiEr29xqapkvdpEzX7lE7jYXZB7WJ9reIy5/i70SgTQHMBXge0A0WhSoN98Xyo3PAXkVkVcrcA8EM0FQeH1wAqy/jKoBGJbvlMakyMjIqBQcFfTx92rQ5P82YsW7G9OkzT5086Yiv0yMrtcvvpStk1pNDT4Ip9O68/X8r1Xi5VDR7eJkLUN5Oo1qFnq8iZF3KDdrIy8f4azk3ZAYMR9pjhG9y1PUUnLBCrJ4/TOuUpBv++eeLzh06XIQ8UAhaBI117bq8nY1NckR4eHN6Pb2XatOfQs4PA5JW8Qi8cfJWWl18nYgydzMXoDwoLCRePOkQojXkeYUqbu6uSwPrfx12yNIzOL7tL3v9/E7caS22ThGC5GiSZWVlccOGDFkEJzwd7R0g20zxkfRWzZsXwAnQPr2+wBmd6bFkQTr0yg2mhF3kBm3i5+yI/yEgWoEAAAsJSURBVAI+E4MDJQRzAUwBtcH8tm5tceHCBRvakfjMOP9EY6bmFsrupOVZkmVBmRhJCjMGzXh35swZ6/98/Ek45PpCBFUZyP2khqIOLZyc4x4+fEjbqtjT9/I9PZ7rv4Hvt/zor0UacU35FMwFMLXz4FWhUFT2mjBhXIC/Py7i9SyvV2wgJTmxxtu4fn13RMLbQFJIjwPpOg1loWlkY8t/9p//7ubJ7KImgxpN828i5ynLZkpoTGJqHv5MjAOXuQCmgpJVqVRajR4xcun4ceO+g/Pl6DMZ1a6sZXye30YzG6anp8u8J3nNRDaoqmnjxpCBprSU5NpWzZqrqld5g1/xx7IBcC8xFyzUqC3em7d/PZrys3ZffNgQfydSO525AOXtUHpm/Ksvv/yra6fO+25cv44zV2t08UXRdYag2DG3OyKiVZcOHaNx4lwnJ7VTk9JrOUE+L8hGM9bV9XfIWMPDgCUO1MboxM+5wZuK1hy9KfrDj8wFKC+E4RrXUaMW1KpWXbFs6dLRGRkZxR3/os+WvwzAoNKQBGRpaWmVpk72nmZTzzoPUseDo1Rq0jmkYSFzd82q1fixo0fPVepIKqdLofcz82vIR25NnLX9Asl8Yp4boo0FcwGeByTxliXks/rphxljIMvch++/fxhppJa0QLC5EhYTlAw0cGw2bdjwSef2Hc480aKOpU31UBFPBV6+g53d/aWLF/cpKMAncNHvBE9ft7usxQ/hW3osiZyr1nn/lmJYfSoLzAV4XgBZqXccHBTU1ca6/oMaVary49zc/7xw/rwtuUamNgOTgC59EoKCTNzePXta9e7VKxQSjSEtqiVa1FBiZC1O89ikCU5C17tnT7/4+HhcUYZmo6EnGib5nh77wW+HFun+J/LwRU5SAHMBXhQBaE74a9eu1fvkw4/Cq1Wuwjexa5SGCLvozOnT9uRanP8KiP0qtSykvQEniWY/hBkgIjy8NSLbFkTQQsjiXJYtCjUP4HsogovInPTnihUDSSxZTiIExaG5TbG32vX9M/pr2M6HCFphNo0zF+AFEwJrKoVCIZvq7T29ft26RZA4GHnN6SOHDlsdEhzcnnQwBjgvQFq1XpXrFwGSg6tE2Czl8WM50vof9OvdJwgRtIAQVEPrhZaSrVkNRT1s6tVXD+rff/XFCxfq0d9KBxsNNyWm5FZF2rRHdr6yivDzigDmArwMgpCkXdz27du6dmzX/l+IQTaysdWiV2WPTz499MeSJaNQh1uTXK20M/F9ZDeSnNi4RoFozOLdTMLvYGAcPXq00cwZP3pBFRgoF28kQfE0DyZBl44dYwP8/d+B58GMoJ94jr6/m66o9DCroBIv0kWO14qotONoKu8HDx5UHufm9jMiaS5oJlh+hGVIe9uGaT3/2yNw+R/LhkdHRzfOyMiQkWrXhiDX21IHzzZo78IzcnJyLKKjolpCGvG+X/QORw5Sbv3a2AaFlaVSCQqlkPDKEyEouj4ByTeUppE3xjFU4/yz7PtAIqoJIPYbttNioqOdBvTtGwwZkoEUrZo110JqbzANkAdd0KFN29NDXFzWrli2zG3f3r3dzp071xCZEFVgjR0gJDFoSfgsOztbhkhZFZGy8Z6IiI9+njnTa9SIEZs7tWsf3/DNBkp4NtGePNGQhm1QR0cVEBhSy2NTxaHx9e+++WZsYmIiTiEP5gMdeGUBCCq2pVGJqASggajtCgTb8M8/3d7t+tZhSqLWzVuoEWnx0iQ4K6DJ4Dv0twJN1XcQ6f5FJD40euTIMG8vr2Bk+wYjwu9s17LVkc7tO5xFuI9IWQBaGu4FMwM0J/LQtc2dnVRlaE81yXWvRfdjTx7JdX7mTz+5Xr9+vYqOeLpoRUVxiCSiGgHQrlQrIS3IbVy/4b2B/foH2lrXz4fYJS552ay5EhE3n3jgfBM7e0w6ABAQiAiEhDz1cD39jhTYwHYlqRRiSHNqYU0eyAskhuIb8Cw0OBTIuQr5Z926HkhL09pOEkFfV6ISAtCYY3HpoMjDh5083T1mIa15GYgIpMXkc8Tkg210hUgrFiJyQdU6JSajriCYmhDSUMxTU6wxddDC7ibQnEBOZGoUdX+3WzSyYafGxcbaE6eueMeURNDXnKhCCALvmLBpaalWO8LCuo33GDf/rS6djzeysc0FUlnXKalBCYEpNARa+hm9Dhw3ID4hv6ZNy5Z3kQYPnj3rlwmxMTHOZCMNBsghadCywVwA1iAmAUy5MvI3ru5y6uRJ+1Ur/+w31ct7jsugwSEd2rb99+0uXe5jAteqA7VctZSI5L0aEVuBrkvp0rHjxY+6f7DHe5LXylk/zQTnrOvDhw+rKVWq4v9LwkwvJYZbEcFcAHMCjYXyetVdYGqGitoFBQVVjh07Zrt3z57WBw8c6HRg//6PED5G6IrQFhHSISUlpQa6Tp6fn//U84nmfKWrYhUFzAUwV9BlT0Hc1KT7BRrTki5zsv5NYgZzAcQGQmA5jSIIAH+bZRLcigDmAlQEwHIlJMBVIsA2Ow3sIUDvAVLayxcD5gKIGbASRDI0y+iyZXa+EoN+D69A3Iq29v6qwVwAsUKDj8Pwci2vhbRCDSb7nXbvteLoRqcZ4ZFNfwiP6r0yatuXq2N/DDx1ty0kzuXhiIhIzyuZA5gLIEYIz8v3WRX9IzdqazpkZOGG+WohrxPGUF9eNmwLLtvoOCM88PBV8604IgYwF0BsoDbnwSuPalWfEHIQcltZeARqSClxpdwjUAWAvFcEGsj3VHlswOOFuy+/D/dqTCheLEEH5gKICVST3kzJtarpGXQQ0jdaegYV6adn14cFIrCFWwDPfeWTHnb2fjt4hqRZTQNzAcQEepy517KjcyC/KC52WwZBS9QWgNLtYwP4muODj91IyaWpiCQHy0gwF0AsoFP+vvjkRoikGYYKXTwLiNgqsGXHbz41kjxTtOfsXzWYCyAW0PpM4zeenIJrshoocvFsrRqohnI5Tb7dsS9PFwmokLvxXwaYCyAWUAfo3bn7tkM5SwuPp+sGGAGdBh7j/+jag6x65LnS9G8EmAsgBpBlURlowSbTw0+DVsTasbxERY7VteScFvBsKQJgHJgLIAbQrM03HudyMo/A+FJKBhkPTNRsiagmgLkAYgEl1DsLDhzFU//TtViN16hjAwqvPcxuInyuhLLBXACxQENCU58ujlwBAXyLcjhTulqtgXwlz+AriSm5FuS5ko1qBJgLIBbQVI4r9l/rgb1+zyCTp35cCwtp449+PbQMnkXJL+HZYC6AWED3mT7KLuCsPYPjOFe/p6pbP2vax2XaB2/K94u73RaeJa1OGQ/mAogJxdWtj9x4nxvmC2v6uunc6GD/Fn7AiqhfaNkhKYZqPJgLIDbQ1STX9SdcoYCDpW55tEQhNn27FNdqdfHhHb4O803J1VV0kU4CmAbmAogNoAU1uDwOzy0/mNATadZHeIsf1LZ6OgyllUOlvIEbNQNWxcxOzyNZoSWSmgzmAogV1BE6fjOtXs8VUbOrjQtKQFO7WjbMF/al4v2olqP8HrWetWfT+ujEdio1JicQXPLyywHmAogV5BhKcV3Su+kKy8irj1vE3kjtHnM99YOj11I6nb2bWY1er5aOozwXmAsgdiCyykjhMYMkhNTkOkKzl1XMYC5ARQHYnUrNk9OnQE7pBOqLA3MBJEgwBswFkCDBGDAXQIIEY8BcAAkSjAFzASRIMAbMBZAgwRgwF0CCBGPAXAAJEowBcwEkSDAGzAWQIMEYMBdAggRj8H8r1p9Mhq0RhwAAAABJRU5ErkJggg==';

export const BotMessage = ({ msg, id }: { msg: string[] | string, id?: string }) => {
	if (!_.isArray(msg)) {
		msg = [msg];
	}
	return (
		<div id={id} style={{ marginBottom: 15 }}>
			{msg.map((m, i) => {
				return (
					<div
						id ={`${id}-${i}`}
						style={{
							marginBottom: 2,

							display: "flex",
							justifyContent: "flex-start",
							alignItems: "center"
						}}
						key={i}
					>
						<img
							id ={`${id}-${i}-avatar`}
							src={tllogo}
							style={{
								marginRight: 10,
								width: 35,
								visibility: i !== 0 ? "hidden" : "visible"
							}}
						/>
						<Paper
							elevation={1}
							style={{
								padding: "10px 15px",
								backgroundColor: "#3f51b5",
								maxWidth: "75%",

								borderRadius:
									i === 0 && msg.length > 1
										? "20px 20px 20px 4px"
										: i === msg.length - 1 && msg.length > 1
											? "4px 20px 20px 20px"
											: msg.length === 1
												? "20px"
												: "4px 20px 20px 4px"
							}}
						>
							<Typography
								variant="body1"
								component="div"
								style={{ color: "white" }}
							>
								<div
									id ={`${id}-${i}-content`}
									dangerouslySetInnerHTML={{
										__html: m
									}}
								/>
							</Typography>
						</Paper>
					</div>
				);
			})}
		</div>
	);
};

export const UserMessage = ({ msg, id }: { msg: string[] | string, id?: string }) => {
	if (typeof msg === "string") {
		msg = [msg];
	}
	return (
		<div id={id} style={{ marginBottom: 15 }}>
			{msg.map((m, i) => (
				<div
				  id={`${id}-${i}`}
					style={{
						display: "flex",
						marginBottom: 5,
						justifyContent: "flex-end",
						alignItems: "center"
					}}
					key={i}
				>
					<Paper
						key={i}
						elevation={1}
						style={{
							padding: 10,
							maxWidth: "74%",
							textAlign: "right",
							backgroundColor: "#DEE6F1",
							borderRadius:
								i === 0 && msg.length > 1
									? "20px 20px 20px 4px"
									: i === msg.length - 1 && msg.length > 1
										? "4px 20px 20px 20px"
										: "20px"
						}}
					>
						<Typography variant="body1" component="div">
							<div
								id={`${id}-${i}-content`}
								dangerouslySetInnerHTML={{
									__html: m
								}}
							/>
						</Typography>
					</Paper>
					<Typography
						style={{
							backgroundColor: "#3B52B0",
							borderRadius: "50%",
							marginLeft: 10,
							width: 45,
							height: 45,
							display: "flex",
							alignItems: "center",
							justifyContent: "center",
							color: "white",
							fontWeight: 900
						}}
						variant="body2"
					>
						U
					</Typography>
				</div>
			))}
		</div>
	);
};
