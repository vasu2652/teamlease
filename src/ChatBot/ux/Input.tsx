import Button from "@material-ui/core/Button";

import Icon from "@material-ui/core/Icon";
import Radio from "@material-ui/core/Radio";
import SendIcon from "@material-ui/icons/Send";
// import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { Input, Typography } from "@material-ui/core";

import { withStyles } from '@material-ui/core/styles';

import _ from "lodash";
import React from "react";

// const styles = theme => ({
//   fab: {
//     margin: theme.spacing.unit,
//   },
//   extendedIcon: {
//     marginRight: theme.spacing.unit,
//   },
// });

export const OptionQuestion = ({ onChange, options }) => {
  return (
    <div>
      <FormControl>
        {/* <RadioGroup
          name={name}
        > */}
          {
            options.map(({ option, value }) => {
              return (
                <FormControlLabel
                key={value}
                value={value}
                onChange={() => onChange(value)}
                control={<Radio />}
                label={option} />

              )
            })
          }
        {/* </RadioGroup> */}
      </FormControl>
      {/* <Typography variant="body2">{question}</Typography>
      <Fab key={value} variant="extended" aria-label="Delete"
        onClick={() => onChange(value)}>
        {option}
      </Fab>
			*/}
      {/* <Typography variant="button">
				{options.map(o => (
					<label key={o.value}>
						<Radio
							name={name}
							value={o.value}
							onChange={() => onChange(o.value)}
						/>
						{o.option}
					</label>
				))}
			</Typography> */}
    </div>
  );
};

export class RatingQuestion extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      hover: {},
      values: {}
    };
  }
  onChange = () => {
    // Check if all values are present.
    if (
      Object.keys(this.state.values).length === this.props.options.length
    ) {
      this.props.onChange(
        Object.keys(this.state.values).map(o => ({
          name: o,
          value: this.state.values[o]
        }))
      );
    }
  };
  rating(o) {
    const ratings = _.range(1, 6).map(n => {
      return (
        <Icon
          key={n}
          onMouseOver={() =>
            this.setState({
              hover: {
                ...this.state.hover,
                [o]: n
              }
            })
          }
          onClick={() => {
            this.setState(
              {
                values: {
                  ...this.state.values,
                  [o]: n
                }
              },
              this.onChange
            );
          }}
        >
          {(this.state.values[o] || this.state.hover[o]) >= n
            ? "star"
            : "star_border"}
        </Icon>
      );
    });
    return (
      <div
        style={{ display: "flex", alignItems: "center", width: 200 }}
        onMouseOut={() => {
          this.setState({
            hover: {
              ...this.state.hover,
              [o]: -1
            }
          });
        }}
      >
        <Typography
          style={{
            flexGrow: 1,
            textAlign: "right",
            paddingRight: 15
          }}
        >
          {o}
        </Typography>
        {ratings}
      </div>
    );
  }
  render() {
    return (
      <div style={{ display: "block" }}>
        {this.props.options.map(o => (
          <div key={o}>{this.rating(o)}</div>
        ))}
      </div>
    );
  }
}


export class TextQuestion extends React.Component<{ onChange: any }, { value: string }>  {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    }
  }
  onChange = (e) => {
    this.setState({
      value: e.target.value,
    })
  }
  onSubmit = () => {
    const { value } = this.state;
    (this.props as any).onChange(value);
    // onChange((this.state as any).value);
  }
  render() {
    const { value } = this.state;
    return (
      <label style={{ flexGrow: 1 }}>
        {/* <Typography variant="body2" style={{ textAlign: "center" }}>
  				{question}
  			</Typography> */}
        <div
          style={{
            display: "flex",
            width: "100%",
            borderRadius: "20px",
            backgroundColor: "",
            padding: 8,
            alignItems: "center"
            // paddingBottom: 0
          }}
        >
          <Input
            name={name}
            autoFocus
            style={{ width: "100%", borderRadius: "20px" }}
            inputProps={{
              style: {
                textAlign: "center",
                width: "calc(100%)",
                borderRadius: "20px"
              }
            }}
            onChange={this.onChange}
            value={value}

            onKeyUp={e => {
              if (e.key === "Enter") {
                this.onSubmit();
                {/* onChange((e.target as any).value); */}
              }
            }}
          />
          <Button onClick={this.onSubmit}>
            <SendIcon />
          </Button>
        </div>
      </label>
    )
  }
}
// export const TextQuestion = ({ onChange }) => {
//   return (
//
//   );
// };
